//
//  AppManager.swift
//  COPD
//
//  Created by Anand Prakash on 21/10/19.
//  Copyright © 2019 Anand Prakash. All rights reserved.
//

import Foundation
import UIKit

class AppManager: NSObject {
    
    // MARK: - Singleton Instance
    static let shared = AppManager()
    
    static let APP_NAME = "Navia"
    static let APP_TYPE = "ios"
    static let APP_USER_TYPE = "patient"
    
    static let UserStoryBoard : UIStoryboard = UIStoryboard (name: "User", bundle: Bundle.main)
    static let DashboardStoryBoard : UIStoryboard = UIStoryboard (name: "Dashboard", bundle: Bundle.main)
    static let StoryBoard : UIStoryboard = UIStoryboard (name: "Main", bundle: Bundle.main)
    
    
    // Web services
    static let Content_Type = "application/json"
    
    static let APP_BASE_URL = "https://www.naviasmart.health/"//LIVE
    //static let APP_BASE_URL = "http://35.226.239.145/"//STAGING


    // MARK: - Init method
    override init() {
        super.init()
       
    }
    
    func getPatientUsername() -> String {

        return UserDefaults.standard.string(forKey: "Username") ?? ""
    }
    
    func getAppAuthenticationToken(_ completion: @escaping (_ token: String?) -> ()) {
        
        if let authToken = UserDefaults.standard.string(forKey: "Authentcation_Token"){
           
            completion(authToken)
        }
        
    }

    func isFirstTimeLogin(_ completion: @escaping (_ isFirstTime: Bool) -> ()) {

        if UserDefaults.standard.string(forKey: "Authentcation_Token") != nil {
            completion(false)
            return
        }
        else{
            completion(true)
        }
    }
    
    func logout() {
    
        // Reset Userdefaults
        resetDefaults()
    }
    
    func resetDefaults() {
        let defaults = UserDefaults.standard
        let dictionary = defaults.dictionaryRepresentation()
        dictionary.keys.forEach { key in
            defaults.removeObject(forKey: key)
        }
    }
}







