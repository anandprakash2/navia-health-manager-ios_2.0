//
//  ServiceModel.swift
//  COPD
//
//  Created by Anand Prakash on 21/10/19.
//  Copyright © 2019 Anand Prakash. All rights reserved.
//

import UIKit
import Foundation
import Alamofire
import Reachability
import SystemConfiguration

class ServiceModel: NSObject {
    
    //MARK: - User API (Login, Update input values, Forgot password)
    
    func login(email:String, password:String, completion:@escaping (NSDictionary) -> ()) {
        if let url = NSURL(string: AppManager.APP_BASE_URL+"navia/app/login/") {
            
            let request = NSMutableURLRequest( url: url as URL)
            
            let signInDict = ["username":email,"password":password,"login_type":AppManager.APP_USER_TYPE]
            
            print(signInDict)
            
            let jsonData = try! JSONSerialization.data(withJSONObject: signInDict, options: JSONSerialization.WritingOptions.init(rawValue: 0))
            
            request.httpMethod = "POST"
            
            request.setValue(AppManager.Content_Type, forHTTPHeaderField: "Content-Type")
            
            request.httpBody = jsonData
            
            let task = URLSession.shared.dataTask(with: request as URLRequest) {
                data, response, error in
                do{
                    if let data = data,
                        let jsonString =  try JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.mutableContainers) as? NSDictionary
                        , error == nil {
                        completion(jsonString)
                    } else {
                        print("error=\(error!.localizedDescription)")
                        let errorDict = ["error_status":true,"message":error!.localizedDescription] as [String : Any]
                        completion(errorDict as NSDictionary)
                        
                    }
                }
                catch{
                    print("error=\(error.localizedDescription)")
                    let errorDict = ["error_status":true,"message":error.localizedDescription] as [String : Any]
                    completion(errorDict as NSDictionary)
                    
                }
                
            }
            task.resume()
        }
    }
    
    
    func checkUsernameExists(phoneNumber:String, isOnlyForPatient:Bool, completion:@escaping (NSDictionary) -> ()) {
        if let url = NSURL(string: AppManager.APP_BASE_URL+"pharma/check_username_exists") {
            
            let request = NSMutableURLRequest( url: url as URL)
            
            var forTypeValue = "patient"
            
            let checkUsernameDict = ["username": phoneNumber, "for":forTypeValue]
            
            print(checkUsernameDict)
            
            let jsonData = try! JSONSerialization.data(withJSONObject: checkUsernameDict, options: JSONSerialization.WritingOptions.init(rawValue: 0))
            
            request.httpMethod = "POST"
            request.setValue(AppManager.Content_Type, forHTTPHeaderField: "Content-Type")
            
            AppManager.shared.getAppAuthenticationToken { (token) in
                
                request.setValue(token, forHTTPHeaderField: "Authentication-Token")
                request.setValue(AppManager.shared.getPatientUsername(), forHTTPHeaderField: "X-username")
                request.setValue(AppManager.APP_USER_TYPE, forHTTPHeaderField: "X-usertype")
            }
            
            request.httpBody = jsonData
            
            let task = URLSession.shared.dataTask(with: request as URLRequest) {
                data, response, error in
                do{
                    if let data = data,
                        let jsonString =  try JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.mutableContainers) as? NSDictionary
                        , error == nil {
                        completion(jsonString)
                    } else {
                        print("error=\(error!.localizedDescription)")
                        let errorDict = ["error_status":true,"message":error!.localizedDescription] as [String : Any]
                        completion(errorDict as NSDictionary)
                        
                    }
                }
                catch{
                    print("error=\(error.localizedDescription)")
                    let errorDict = ["error_status":true,"message":error.localizedDescription] as [String : Any]
                    completion(errorDict as NSDictionary)
                    
                }
                
            }
            task.resume()
        }
    }
    
    
    func sendOTP(phone:String, completion:@escaping (NSDictionary) -> ()) {
        if let url = NSURL(string: AppManager.APP_BASE_URL+"navia/send_otp") {
            
            let request = NSMutableURLRequest( url: url as URL)
            
            let sendOTPDict = ["phone_number":phone]
            
            print(sendOTPDict)
            
            let jsonData = try! JSONSerialization.data(withJSONObject: sendOTPDict, options: JSONSerialization.WritingOptions.init(rawValue: 0))
            
            request.httpMethod = "POST"
            request.setValue(AppManager.Content_Type, forHTTPHeaderField: "Content-Type")
            
            AppManager.shared.getAppAuthenticationToken { (token) in
                
                request.setValue(token, forHTTPHeaderField: "Authentication-Token")
                request.setValue(AppManager.shared.getPatientUsername(), forHTTPHeaderField: "X-username")
                request.setValue(AppManager.APP_USER_TYPE, forHTTPHeaderField: "X-usertype")
            }
            
            request.httpBody = jsonData
            
            let task = URLSession.shared.dataTask(with: request as URLRequest) {
                data, response, error in
                do{
                    if let data = data,
                        let jsonString =  try JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.mutableContainers) as? NSDictionary
                        , error == nil {
                        completion(jsonString)
                    } else {
                        print("error=\(error!.localizedDescription)")
                        let errorDict = ["error_status":true,"message":error!.localizedDescription] as [String : Any]
                        completion(errorDict as NSDictionary)
                        
                    }
                }
                catch{
                    print("error=\(error.localizedDescription)")
                    let errorDict = ["error_status":true,"message":error.localizedDescription] as [String : Any]
                    completion(errorDict as NSDictionary)
                    
                }
                
            }
            task.resume()
        }
    }
    
    func changePassword(newPassword:String, phoneNumber: String, completion:@escaping (NSDictionary) -> ()) {
        if let url = NSURL(string: AppManager.APP_BASE_URL+"pharma/change_password/") {
            
            let request = NSMutableURLRequest( url: url as URL)
            
            var forTypeValue = "patient"
            
            let changePassDict = ["device_id":newPassword, "username": phoneNumber, "for":forTypeValue]
            
            print(changePassDict)
            
            let jsonData = try! JSONSerialization.data(withJSONObject: changePassDict, options: JSONSerialization.WritingOptions.init(rawValue: 0))
            
            request.httpMethod = "POST"
            
            request.setValue(AppManager.Content_Type, forHTTPHeaderField: "Content-Type")
            
            request.setValue(phoneNumber, forHTTPHeaderField: "x-username")
            
            request.setValue(AppManager.shared.getPatientUsername(), forHTTPHeaderField: "X-username")
            
            request.setValue(AppManager.APP_NAME, forHTTPHeaderField: "x-appname")
            request.setValue(AppManager.APP_TYPE, forHTTPHeaderField: "x-apptype")
            
            request.httpBody = jsonData
            
            let task = URLSession.shared.dataTask(with: request as URLRequest) {
                data, response, error in
                do{
                    if let data = data,
                        let jsonString =  try JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.mutableContainers) as? NSDictionary
                        , error == nil {
                        completion(jsonString)
                    } else {
                        print("error=\(error!.localizedDescription)")
                        let errorDict = ["error_status":true,"message":error!.localizedDescription] as [String : Any]
                        completion(errorDict as NSDictionary)
                        
                    }
                }
                catch{
                    print("error=\(error.localizedDescription)")
                    let errorDict = ["error_status":true,"message":error.localizedDescription] as [String : Any]
                    completion(errorDict as NSDictionary)
                    
                }
                
            }
            task.resume()
        }
    }
    
    func createUser(phone:String, name:String, gender:String, dob:String, pin:String, completion:@escaping (NSDictionary) -> ()) {
        if let url = NSURL(string: AppManager.APP_BASE_URL+"navia/user/") {
            
            let request = NSMutableURLRequest( url: url as URL)
            
            var forTypeValue = "register-user"
            
            let createUserDict = ["username":phone,"phone":phone, "device_id":pin, "name":name,"gender":gender, "dob":dob, "for":forTypeValue]
            
            print(createUserDict)
            
            let jsonData = try! JSONSerialization.data(withJSONObject: createUserDict, options: JSONSerialization.WritingOptions.init(rawValue: 0))
            
            request.httpMethod = "POST"
            
            request.setValue(AppManager.Content_Type, forHTTPHeaderField: "Content-Type")
            
            AppManager.shared.getAppAuthenticationToken { (token) in
                
                request.setValue(token, forHTTPHeaderField: "Authentication-Token")
                
                request.setValue(AppManager.shared.getPatientUsername(), forHTTPHeaderField: "X-username")
                
                request.setValue(AppManager.APP_USER_TYPE, forHTTPHeaderField: "X-usertype")
            }
            
            request.httpBody = jsonData
            
            let task = URLSession.shared.dataTask(with: request as URLRequest) {
                data, response, error in
                do{
                    if let data = data,
                        let jsonString =  try JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.mutableContainers) as? NSDictionary
                        , error == nil {
                        completion(jsonString)
                    } else {
                        print("error=\(error!.localizedDescription)")
                        let errorDict = ["error_status":true,"message":error!.localizedDescription] as [String : Any]
                        completion(errorDict as NSDictionary)
                        
                    }
                }
                catch{
                    print("error=\(error.localizedDescription)")
                    let errorDict = ["error_status":true,"message":error.localizedDescription] as [String : Any]
                    completion(errorDict as NSDictionary)
                    
                }
                
            }
            task.resume()
        }
    }
    
    
    //MARK:- Reports & Prescriptions
       
       func fetchReportAndPrescriptions(username:String, completion:@escaping (ReportsModal?) -> ()) {
           
           if let url = NSURL(string: AppManager.APP_BASE_URL+"navia/investigations/?for=doctor&user=\(username)") {
               
               let request = NSMutableURLRequest( url: url as URL)
               request.httpMethod = "GET"
               
               request.setValue(AppManager.Content_Type, forHTTPHeaderField: "Content-Type")
               
               AppManager.shared.getAppAuthenticationToken { (token) in
                   
                   request.setValue(token, forHTTPHeaderField: "Authentication-Token")
                   request.setValue(AppManager.shared.getPatientUsername(), forHTTPHeaderField: "X-username")
                   request.setValue(AppManager.APP_USER_TYPE, forHTTPHeaderField: "X-usertype")
               }
               
               let task = URLSession.shared.dataTask(with: request as URLRequest) {
                   data, response, error in
                   
                   do{
                       if let d = data {
                           let model  = try JSONDecoder().decode(ReportsModal.self, from: d)
                           completion(model)
                           
                       }
                   }
                       
                   catch{
                       print("error=\(error.localizedDescription)")
                       
                       completion(nil)
                       
                   }
                   
               }
               task.resume()
           }
       }
           
        //MARK:- User Profile
           
           func fetchUserProfile(username:String, completion:@escaping (UserDataModel?) -> ()) {
               
               if let url = NSURL(string: AppManager.APP_BASE_URL+"navia/user/?username=\(username)") {
                   
                   let request = NSMutableURLRequest( url: url as URL)
                   request.httpMethod = "GET"
                   
                   request.setValue(AppManager.Content_Type, forHTTPHeaderField: "Content-Type")
                   
                   AppManager.shared.getAppAuthenticationToken { (token) in
                       
                       request.setValue(token, forHTTPHeaderField: "Authentication-Token")
                       request.setValue(AppManager.shared.getPatientUsername(), forHTTPHeaderField: "X-username")
                       request.setValue(AppManager.APP_USER_TYPE, forHTTPHeaderField: "X-usertype")
                   }
                   
                   let task = URLSession.shared.dataTask(with: request as URLRequest) {
                       data, response, error in
                       
                       do{
                        

                           if let d = data {
                            let responseString = String(data: d, encoding: .utf8)
                            if responseString != nil {
                              print("Response string:\(responseString!)")
                            }
                               let model  = try JSONDecoder().decode(UserDataModel.self, from: d)
                               completion(model)
                               
                           }
                       }
                           
                       catch{
                           print("error=\(error.localizedDescription)")
                           
                           completion(nil)
                           
                       }
                       
                   }
                   task.resume()
               }
           }
    

    func editUserProfile(username:String, name:String, dob:String, gender:String, completion:@escaping (NSDictionary) -> ()) {
        if let url = NSURL(string: AppManager.APP_BASE_URL+"navia/edit_profile/") {
            
            let request = NSMutableURLRequest( url: url as URL)
            
            
            let createUserDict = ["username": username, "name":name,"gender":gender, "dob":dob]
            
            print(createUserDict)
            
            let jsonData = try! JSONSerialization.data(withJSONObject: createUserDict, options: JSONSerialization.WritingOptions.init(rawValue: 0))
            
            request.httpMethod = "POST"
            
            request.setValue(AppManager.Content_Type, forHTTPHeaderField: "Content-Type")
            
            AppManager.shared.getAppAuthenticationToken { (token) in
                
                request.setValue(token, forHTTPHeaderField: "Authentication-Token")
                
                request.setValue(AppManager.shared.getPatientUsername(), forHTTPHeaderField: "X-username")
                
                request.setValue(AppManager.APP_USER_TYPE, forHTTPHeaderField: "X-usertype")
            }
            
            request.httpBody = jsonData
            
            let task = URLSession.shared.dataTask(with: request as URLRequest) {
                data, response, error in
                do{
                    if let data = data,
                        let jsonString =  try JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.mutableContainers) as? NSDictionary
                        , error == nil {
                        completion(jsonString)
                    } else {
                        print("error=\(error!.localizedDescription)")
                        let errorDict = ["error_status":true,"message":error!.localizedDescription] as [String : Any]
                        completion(errorDict as NSDictionary)
                        
                    }
                }
                catch{
                    print("error=\(error.localizedDescription)")
                    let errorDict = ["error_status":true,"message":error.localizedDescription] as [String : Any]
                    completion(errorDict as NSDictionary)
                    
                }
                
            }
            task.resume()
        }
    }
    
    //MARK:-  Custom Alert
    
    func showAlert(title: String, message: String, parentView:UIViewController) -> Void {
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        let destructiveAction  = UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: { (result:UIAlertAction) -> Void in
        })
        alert.addAction(destructiveAction)
        parentView.present(alert, animated: true, completion: nil)
        
    }
    
    //MARK:-  Handle error alert
    
    func handleError(error: Error, parentView:UIViewController) -> Void {
        
        let alert = UIAlertController(title: "Error",
                                      message: error.localizedDescription,
                                      preferredStyle: .alert)
        
        let okAction = UIAlertAction(title: "OK",
                                     style: .default,
                                     handler: { (action) in
                                        
                                        alert.dismiss(animated: true, completion: nil)
        })
        
        alert.addAction(okAction)
        parentView.present(alert, animated: true, completion: nil)
    }
    
}


/// checking the network reachability functionality
public class NetworkConnection {
    
    class func isConnectedToNetwork() -> Bool {
        
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(MemoryLayout<sockaddr_in>.size)
        zeroAddress.sin_family = sa_family_t(AF_INET)
        
        guard let defaultRouteReachability = withUnsafePointer(to: &zeroAddress, {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {
                SCNetworkReachabilityCreateWithAddress(nil, $0)
            }
        }) else {
            return false
        }
        
        var flags: SCNetworkReachabilityFlags = []
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability, &flags) {
            return false
        }
        
        let isReachable = flags.contains(.reachable)
        let needsConnection = flags.contains(.connectionRequired)
        
        return (isReachable && !needsConnection)
        
    }
}

extension String {
    func appendLineToURL(fileURL: URL) throws {
        try (self + "\n").appendToURL(fileURL: fileURL)
    }
    
    func appendToURL(fileURL: URL) throws {
        let data = self.data(using: String.Encoding.utf8)!
        try data.append(fileURL: fileURL)
    }
}

extension Data {
    func append(fileURL: URL) throws {
        if let fileHandle = FileHandle(forWritingAtPath: fileURL.path) {
            defer {
                fileHandle.closeFile()
            }
            fileHandle.seekToEndOfFile()
            fileHandle.write(self)
        }
        else {
            try write(to: fileURL, options: .atomic)
        }
    }
}
