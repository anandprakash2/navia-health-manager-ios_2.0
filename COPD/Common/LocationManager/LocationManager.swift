//
//  LocationManager.swift
//  JukeBaux
//
//  Created by Suraj on 03/06/18.
//  Copyright © 2018 Suraj. All rights reserved.
//

import Foundation
import CoreLocation

class LocationManager: NSObject {
    
    static let shared = LocationManager()
    
    let locationManager = CLLocationManager()
    var latitude: String?
    var longitude: String?
    var coordinate: CLLocationCoordinate2D?
    var pendingLocationUpdateClousure: (() -> ())?
    var timer = Timer()
    
    override init() {
        super.init()
        startUpdateTimer()
    }
    
    func syncLocation(completion: @escaping (_ updatedCoordinate: CLLocationCoordinate2D?) -> ()) {
        // Ask for Authorisation from the User.
        self.locationManager.requestAlwaysAuthorization()
        
        // For use in foreground
        self.locationManager.requestWhenInUseAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.startUpdatingLocation()
            
            pendingLocationUpdateClousure = {
                completion(self.coordinate)
                self.pendingLocationUpdateClousure = nil
            }
        }
    }
    
    func getCoordinate(completion: @escaping (_ coordinate: CLLocationCoordinate2D?) -> ()) {
        
        if let coordinate = self.coordinate {
            completion(coordinate)
            return
        }
        
        syncLocation { (coordinate) in
            completion(coordinate)
        }
    }
    
    func startUpdateTimer() {
        
        timer.invalidate()
        timer = Timer.scheduledTimer(timeInterval: 10,
                                     target: self,
                                     selector: #selector(self.timerFired),
                                     userInfo: nil, repeats: true)
    }
    
    func stopTimer() {
        timer.invalidate()
    }
    
    @objc func timerFired() {
        
        syncLocation { (coordinate) in

        }
    }
}

// MARK: - CLLocationManagerDelegate methods
extension LocationManager: CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager,
                         didUpdateLocations locations: [CLLocation]) {
        
        coordinate = manager.location?.coordinate
        manager.stopUpdatingLocation()
        pendingLocationUpdateClousure?()
    }
}

extension CLLocationCoordinate2D {
    
    func getLat() -> String {
        return "\(self.latitude)"
    }
    
    func getLong() -> String {
        return "\(self.longitude)"
    }
}
