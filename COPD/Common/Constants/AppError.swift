//
//  AppError.swift
//  COPD
//
//  Created by Anand Prakash on 17/10/19.
//  Copyright © 2019 Anand Prakash. All rights reserved.
//

import Foundation

enum AppError: Error, LocalizedError {
    
    // MARK: - General Errors
    case loginFailed
    case wrapMessage(message: String)
    case wrap(error: Error)
    
    var errorDescription: String? {
        switch self {
        case .loginFailed:              return "Login failed.. Please try again."
        case let .wrapMessage(message): return message
        case let .wrap(error):          return error.localizedDescription
        default:                        return "Unknown Error"
        }
    }
    
    
    // MARK: - Login Screen Errors
    enum LoginScreenError: Error, LocalizedError {
        
        case usernameFieldEmpty
        case emailFieldEmpty
        case invalidEmail
        case passwordFieldEmpty
        case genderFieldEmpty
        case dosageFieldEmpty
        case weightFieldEmpty
        case heightFieldEmpty
        case dobFieldEmpty
        case invalidDOB
        case pinFieldEmpty
        case invalidPin
        case invalidLoginPin
        case invalidName
        case newPinFieldEmpty
        case confirmPinFieldEmpty
        case phoneFieldEmpty
        case questionFieldEmpty
        case invalidPhoneNumber
        case queryFieldEmpty
        case docNameFieldEmpty
        
        var errorDescription: String? {
            switch self {
            case .usernameFieldEmpty:        return "Please enter your name"
            case .emailFieldEmpty:           return "Please enter the email"
            case .weightFieldEmpty:          return "Please enter the weight"
            case .heightFieldEmpty:          return "Please enter the height"
            case .passwordFieldEmpty:        return "Please enter the password"
            case .genderFieldEmpty:          return "Please select your gender"
            case .dosageFieldEmpty:          return "Please select your dosage"
            case .dobFieldEmpty:             return "Please select your date of birth"
            case .invalidDOB:                return "You must be 18 years of age or older. Sorry, this is required by law."
            case .pinFieldEmpty:             return "Please enter the secure pin"
            case .invalidPin:                return "Pin should be 6 characters long"
            case .invalidLoginPin:           return "Pin should be minimun 4 and maximum 6 characters long"
            case .invalidName:               return "Name should be more than 3 characters"
            case .newPinFieldEmpty:          return "Please enter a new 6-digit secure pin"
            case .confirmPinFieldEmpty:      return "Please enter confirm 6-digit secure pin"
            case .phoneFieldEmpty:           return "Please enter the phone number"
            case .questionFieldEmpty:        return "Please give your answer"
            case .invalidEmail:              return "Invalid Email"
            case .invalidPhoneNumber:        return "Invalid Phone number"
            case .queryFieldEmpty:           return "Please enter your query."
            case .docNameFieldEmpty:         return "Please enter the name."

            }
        }
    }

}


