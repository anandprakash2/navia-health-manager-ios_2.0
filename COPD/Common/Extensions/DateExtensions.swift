//
//  DateExtensions.swift
//  NaviaDoctors
//
//  Created by Sumit Mishra on 11/02/20.
//  Copyright © 2020 IOS Development - NAvia. All rights reserved.
//

import Foundation


extension Date {
    

    var currentDate: String {
        let date = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        return dateFormatter.string(from: date)
    }
    
    var sevenDaysBeforeFromCurrentDate: String {
        let date = Calendar.current.date(byAdding: .day, value: -7, to: self)!
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        return dateFormatter.string(from: date)
    }
    
    var before30DayFromCurrentDate: String {
        let date = Calendar.current.date(byAdding: .day, value: -30, to: self)!
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        return dateFormatter.string(from: date)
    }
    
    var after30DayFromCurrentDate: String {
        let date = Calendar.current.date(byAdding: .day, value: 30, to: self)!
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        return dateFormatter.string(from: date)
    }
  
    func generatePreviousSevenDays() -> [String] {
        var sevenDates = [String]()
        sevenDates.append(currentDate)
        for i in 1...6{
            let date = Calendar.current.date(byAdding: .day, value: -i, to: self)!
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd"
            sevenDates.append(dateFormatter.string(from: date))
        }
        return sevenDates
    }
    
    
    func generatePreviousThirtyDays() -> [String] {
        var thirtyDates = [String]()
        for i in 1...29{
            let date = Calendar.current.date(byAdding: .day, value: -i, to: self)!
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd"
            thirtyDates.append(dateFormatter.string(from: date))
        }
        return thirtyDates
    }
    
   static func formateDate(date: Date)-> String {
        let formattor = DateFormatter()
        formattor.dateFormat = "YYYY-MM-dd"
        let stringDate = formattor.string(from: date)
        return stringDate
    }
  
    static func displayDateFormate(_ dateString: String?)-> String? {
        guard let dateString = dateString else { return nil }
        let dateFormatter = DateFormatter()
        let dateFormates = ["dd/MM/yyyy","dd-MM-yyyy","YYYY-MM-dd","YYYY/MM/dd"]
        
        if dateString.count > 5 {
            let thirsCharacter = Array(dateString)[2]
            switch thirsCharacter {
            case "/":
                dateFormatter.dateFormat = dateFormates[0]
            case "-":
                dateFormatter.dateFormat = dateFormates[1]
            default:
                break
            }
            let forthCharacter = Array(dateString)[4]
            switch forthCharacter {
            case "-":
                dateFormatter.dateFormat = dateFormates[2]
            case "/":
                dateFormatter.dateFormat = dateFormates[3]
            default:
                break
            }
        }
        
        guard let dateFromString = dateFormatter.date(from: dateString) else { return nil}
        dateFormatter.dateFormat = "dd-MM-yyyy"
        return dateFormatter.string(from: dateFromString)
    }
    
    
    static func displayDateFormatForDayTab(_ dateString: String) -> String
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-mm-dd"
        let date = dateFormatter.date(from: dateString)
        dateFormatter.dateFormat = "yyyy-mm-dd" //dd MMMM yyyy
        return dateFormatter.string(from: date!)
        
    }
        
    
    static func formateTime(timeString: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "HH:mm"
        guard let date = dateFormatter.date(from: timeString) else {return ""}
        dateFormatter.dateFormat = "h:mm a"
        let amPm = dateFormatter.string(from: date)
        return amPm
    }
    
    
    static func convertTimeIn24Hours(timeString: String) -> String {
         let dateFormatter = DateFormatter()
         dateFormatter.dateFormat = "h:mm a"
         guard let date = dateFormatter.date(from: timeString) else {return ""}
         dateFormatter.dateFormat = "HH:mm"
         let time = dateFormatter.string(from: date)
         return time
    }
    
    
  static func convert24HoursToTimeInterval (_ stime: String?)-> Double {
        guard  var time = stime else { return 0 }
        time = time.replacingOccurrences(of: ":", with: ".")
        let timeAsDouble = (time as NSString).doubleValue
        return timeAsDouble * 3600
    }

    
    
   static func convertGiveDateToTimeIntervalSince1970(_ dateString: String?)-> Double? {
        guard let dateString = dateString else { return nil }
        let dateFormatter = DateFormatter()
        let dateFormates = ["dd/MM/yyyy","dd-MM-yyyy","YYYY-MM-dd","YYYY/MM/dd"]
        
        if dateString.count > 5 {
            let thirdCharacter = Array(dateString)[2]
            switch thirdCharacter {
            case "/":
                dateFormatter.dateFormat = dateFormates[0]
            case "-":
                dateFormatter.dateFormat = dateFormates[1]
            default:
                break
            }
            let forthCharacter = Array(dateString)[4]
            switch forthCharacter {
            case "-":
                dateFormatter.dateFormat = dateFormates[2]
            case "/":
                dateFormatter.dateFormat = dateFormates[3]
            default:
                break
            }
        }
        
        guard let date = dateFormatter.date(from: dateString) else { return nil}
        return date.timeIntervalSince1970
        
    }
    
    //First Date Of The Month
    static func getFirstDateOfTheMonth(index: Int) -> Date? {
        let components:NSDateComponents = Calendar.current.dateComponents([.year, .month], from: Date()) as NSDateComponents
        
        if index == 0{
            components.month -= 1
        }
        else if index == 2{
            components.month += 1
        }
        
        return Calendar.current.date(from: components as DateComponents)!
    }
    
    
    //Last Date Of The Month
    static func getLastDateOfTheMonth(index: Int) -> Date? {
        
        let components:NSDateComponents = Calendar.current.dateComponents([.year, .month], from: Date()) as NSDateComponents
        
        if index == 1{
            
            components.month += 1
        }
        else if index == 2{
            
            components.month += 2
        }
        
        components.day = 1
        components.day -= 1
        
        return Calendar.current.date(from: components as DateComponents)!
    }
    
    static func startDateOfWeek(index: Int) -> Date? {
        let gregorian = Calendar(identifier: .gregorian)
        guard let sunday = gregorian.date(from: gregorian.dateComponents([.yearForWeekOfYear, .weekOfYear], from: Date())) else { return nil }
        return gregorian.date(byAdding: .day, value: 1, to: sunday)
    }
    
    static func endDateOfWeek(index: Int) -> Date? {
        let gregorian = Calendar(identifier: .gregorian)
        guard let sunday = gregorian.date(from: gregorian.dateComponents([.yearForWeekOfYear, .weekOfYear], from: Date())) else { return nil }
        return gregorian.date(byAdding: .day, value: 7, to: sunday)
    }
    
}
