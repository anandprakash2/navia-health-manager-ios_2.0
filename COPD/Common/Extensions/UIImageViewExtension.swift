//
//  UIImageViewExtension.swift
//  COPD
//
//  Created by Anand Prakash on 17/10/19.
//  Copyright © 2019 Anand Prakash. All rights reserved.
//

import Foundation
import AlamofireImage
import UIKit

extension UIImageView {
    
    func addRoundedCorner() {
        self.cornerRadius = self.frame.size.width / 2
        self.layer.masksToBounds = true
        self.clipsToBounds = true
    }
    
    public func medicineImageFromURL(urlString: String, ignoreCache: Bool) {
        
        
        if ignoreCache {
            self.af_setMedicineImageIgnoreCache(string: urlString)
        } else {
            
            if let url = URL(string: urlString) {
                self.af_setImage(withURL: url,
                                 placeholderImage: #imageLiteral(resourceName: "no_image.png"),
                                 imageTransition: .crossDissolve(0.5))
            } else {
                self.image = #imageLiteral(resourceName: "no_image.png")
            }
        }
    }
    
    func af_setMedicineImageIgnoreCache(string: String?) {
        guard let url = string, let nsurl = URL(string: url) else { return }
        let urlRequest = URLRequest(url: nsurl, cachePolicy: .reloadIgnoringLocalCacheData)
        
        let imageDownloader = ImageDownloader.default
        if let imageCache = imageDownloader.imageCache as? AutoPurgingImageCache, let urlCache = imageDownloader.sessionManager.session.configuration.urlCache {
            _ = imageCache.removeImages(matching: urlRequest)
            urlCache.removeCachedResponse(for: urlRequest)
        }
        
        af_setImage(withURLRequest: urlRequest,
                    placeholderImage: #imageLiteral(resourceName: "no_image.png"),
                    imageTransition: .crossDissolve(0.5))
    }
    
    
    public func medicationImageFromURL(urlString: String, ignoreCache: Bool) {
        
        
        if ignoreCache {
            self.af_setMedicationImageIgnoreCache(string: urlString)
        } else {
            
            if let url = URL(string: urlString) {
                self.af_setImage(withURL: url,
                                 placeholderImage: UIImage(),
                                 imageTransition: .crossDissolve(0.5))
            } else {
                self.image = UIImage()
            }
        }
    }
    
    func af_setMedicationImageIgnoreCache(string: String?) {
        guard let url = string, let nsurl = URL(string: url) else { return }
        let urlRequest = URLRequest(url: nsurl, cachePolicy: .reloadIgnoringLocalCacheData)
        
        let imageDownloader = ImageDownloader.default
        if let imageCache = imageDownloader.imageCache as? AutoPurgingImageCache, let urlCache = imageDownloader.sessionManager.session.configuration.urlCache {
            _ = imageCache.removeImages(matching: urlRequest)
            urlCache.removeCachedResponse(for: urlRequest)
        }
        
        af_setImage(withURLRequest: urlRequest,
                    placeholderImage: UIImage(),
                    imageTransition: .crossDissolve(0.5))
    }
    
    
    public func imageFromURL(urlString: String, ignoreCache: Bool) {
        
        
        if ignoreCache {
            self.af_setImageIgnoreCache(string: urlString)
        } else {
            
            if let url = URL(string: urlString) {
                self.af_setImage(withURL: url,
                                 placeholderImage: #imageLiteral(resourceName: "placeholder.png"),
                                 imageTransition: .crossDissolve(0.5))
            } else {
                self.image = #imageLiteral(resourceName: "RoaryselectedIcon")
            }
        }
    }
    
    func af_setImageIgnoreCache(string: String?) {
        guard let url = string, let nsurl = URL(string: url) else { return }
        let urlRequest = URLRequest(url: nsurl, cachePolicy: .reloadIgnoringLocalCacheData)
        
        let imageDownloader = ImageDownloader.default
        if let imageCache = imageDownloader.imageCache as? AutoPurgingImageCache, let urlCache = imageDownloader.sessionManager.session.configuration.urlCache {
            _ = imageCache.removeImages(matching: urlRequest)
            urlCache.removeCachedResponse(for: urlRequest)
        }
        
        af_setImage(withURLRequest: urlRequest,
                    placeholderImage: #imageLiteral(resourceName: "placeholder"),
                    imageTransition: .crossDissolve(0.5))
    }
}
