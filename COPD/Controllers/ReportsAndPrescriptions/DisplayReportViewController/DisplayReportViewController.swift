//
//  DisplayReportViewController.swift
//  Navia Health Manager
//
//  Created by Kapil Rathore on 05/12/17.
//  Copyright © 2017 Navia Life Care. All rights reserved.
//

import UIKit
import WebKit

class DisplayReportViewController: UIViewController {
    
    var urlLink: String = ""
    var docType: String = ""
    
    @IBOutlet weak var lblTitle: UILabel!
    
    @IBOutlet weak var webView: WKWebView! {
        didSet {
            self.webView.backgroundColor = UIColor.white
            self.webView.navigationDelegate = self
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        lblTitle.text = docType
    }
    
    override func viewDidAppear(_ animated: Bool) {
        HUDIndicatorView.shared.showHUD(view: self.view, title: "Please wait ..")
        loadUrlInWebView()
    }
    
    @IBAction func backBtn(_ sender: Any) {
        self.navigationController?.popViewController(animated: false)
    }
    
    func loadUrlInWebView() {
        if self.urlLink != "" {
            let urlRequest = URLRequest(url: URL(string: urlLink)!)
            self.webView.load(urlRequest)
        }
    }

}


extension DisplayReportViewController: WKNavigationDelegate {

    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        HUDIndicatorView.shared.dismissHUD()
    }
    
    func webView(_ webView: WKWebView, didFailNavigation navigation: WKNavigation, withError error: NSError) {
        HUDIndicatorView.shared.dismissHUD()
        ServiceModel().showAlert(title: "Error", message: "Error in loading data. Please try again later.", parentView: self)
    }

}
