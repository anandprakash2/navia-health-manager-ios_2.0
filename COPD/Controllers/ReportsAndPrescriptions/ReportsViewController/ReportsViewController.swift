//
//  ReportsViewController.swift
//  Navia Health Manager
//
//  Created by Anand Prakash on 28/03/20.
//  Copyright © 2020 Anand Prakash. All rights reserved.
//

import UIKit

class ReportsViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var noReportsPresentLabel: UILabel!
    
    var reportsArray = [Reports]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        registerNib()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // Get Reports data
        self.fetchReportPrescriptions()
        
    }
    
    
    func registerNib(){
        
        let customNib = UINib(nibName: "ReportsTableViewCell", bundle: Bundle.main)
        tableView.register(customNib, forCellReuseIdentifier: "ReportsTableViewCell")
        tableView.separatorStyle = .none
        
    }
    
    
    @IBAction func plusBtnClicked(_ sender: Any) {
        
        let optionViewController = AppManager.DashboardStoryBoard.instantiateViewController(withIdentifier: "UploadOptionViewController") as? UploadOptionViewController
        optionViewController!.modalPresentationStyle = .overCurrentContext
        optionViewController!.delegate = self
        self.navigationController?.present(optionViewController!, animated: false, completion: nil)
    }
    
    @IBAction func backBtn(_ sender: Any) {
        self.navigationController?.popViewController(animated: false)
    }
    
}
extension ReportsViewController: UITableViewDataSource, UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return reportsArray.count
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
        
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ReportsTableViewCell", for: indexPath) as! ReportsTableViewCell
        
        let report = self.reportsArray[indexPath.row]
        
        cell.lblTitle.text = report.title
        cell.lblType.text = report.uploadType.map { $0.rawValue }
        cell.lblDate.text = convertDateFormater(report.creationDate!)
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let selectedReport = self.reportsArray[indexPath.row]
        
        if let reportUrl = selectedReport.report {
            let coordinator = DisplayReportCoordinator(root: navigationController!, url: reportUrl, type: selectedReport.uploadType.map { $0.rawValue }!)
            coordinator.start()
        } else {
            ServiceModel().showAlert(title: "Error", message: "Could not load report. Please try again later.", parentView: self)

        }
    }
    
    // Change date format from "yyyy-dd-MM" to "dd-MM-yyyy"
    func convertDateFormater(_ date: String) -> String
    {
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "yyyy-dd-MM"
        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.dateFormat = "dd-MM-yyyy"
        let formatedDate = dateFormatterGet.date(from: date)
        
        return dateFormatterPrint.string(from: formatedDate ?? Date())
    }
    
    // Get Reports data
    func fetchReportPrescriptions(){
        
        let servicemodel = ServiceModel()
        
        if (NetworkConnection.isConnectedToNetwork()){
            
            do {
                
                HUDIndicatorView.shared.showHUD(view: self.view,
                                                title: "Loading ..")
                
                DispatchQueue.global(qos: .userInitiated).async {
                    
                    servicemodel.fetchReportAndPrescriptions(username: AppManager.shared.getPatientUsername()) { responseStr in
                        print(responseStr!)
                        
                        DispatchQueue.main.async {
                            
                            if let status = responseStr?.result {
                                
                                if(status == "SUCCESS"){
                                    
                                    self.reportsArray = responseStr!.data!
                                    
                                    self.reportsArray = self.reportsArray.sorted { (report1, report2) -> Bool in
                                        let formatter = DateFormatter()
                                        formatter.dateFormat = "yyyy-MM-dd"
                                        guard let date1 = formatter.date(from: report1.creationDate ?? "") else { return false }
                                        guard let date2 = formatter.date(from: report2.creationDate ?? "") else { return false }
                                        return date1 > date2
                                    }
                                    
                                    self.tableView.reloadData()
                                    
                                    self.noReportsPresentLabel.isHidden = !(self.reportsArray.count == 0)
                                    
                                    HUDIndicatorView.shared.dismissHUD()
                                    
                                }
                                else{
                                    
                                    HUDIndicatorView.shared.dismissHUD()
                                    
                                    // handle Error or show error message to the user
                                    servicemodel.showAlert(title: status, message: "Failed to load Reports.", parentView: self)
                                }
                                
                            }
                            
                        }
                    }
                }
                
            }
        }
    }
    
}


extension ReportsViewController: OptionViewDelegate{
    func navigateToUploadNewFile(uploadType: String) {
        let uploadReportViewController = self.storyboard?.instantiateViewController(withIdentifier: "UploadReportViewController") as! UploadReportViewController
        
        uploadReportViewController.uploadType = uploadType
        self.navigationController?.pushViewController(uploadReportViewController, animated: false)
    }
}
