//
//  UploadReportViewController.swift
//  Navia Health Manager
//
//  Created by Anand Prakash on 28/03/20.
//  Copyright © 2020 Anand Prakash. All rights reserved.
//

import UIKit
import MobileCoreServices
import Alamofire

class UploadReportViewController: UIViewController {
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblNavTitle: UILabel!
    @IBOutlet weak var lblSubTitle: UILabel!
    
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var tfName: UITextField!
    @IBOutlet weak var btnUpload: UIButton!
    
    var uploadType: String = ""
    var imagePicker = UIImagePickerController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        lblTitle.text = "Upload " + uploadType
        lblNavTitle.text = "Upload " + uploadType
        lblSubTitle.text = "Uploaded " + uploadType + " will be sent to doctor"
        
        tfName.placeholder = "Enter name of " + uploadType
        
        btnUpload.setTitle("Upload Options", for:.normal)
        
        imagePicker.delegate = self
        tfName.delegate = self
    }
    
    @IBAction func backBtn(_ sender: Any) {
        self.navigationController?.popViewController(animated: false)
    }
    
    func getEnteredDocName() throws -> String {
        
        guard let name = tfName.text,
            name != "" else {
                throw AppError.LoginScreenError.docNameFieldEmpty
        }
        
        return name
    }
    
    @IBAction func uploadOptions(_ sender: UIButton) {
        
        if sender.tag == 1{
            sender.tag = 2
            
            btnUpload.setTitle("Upload File", for:.normal)
            
            let alertController = UIAlertController(title: "Navia Health Manager", message: "How do you want to upload report?", preferredStyle: .alert)
            let cameraAction = UIAlertAction(title: "Camera", style: .default) { (action) in
                self.imagePicker.sourceType = .camera
                self.imagePicker.allowsEditing = true
                
                self.present(self.imagePicker, animated: true, completion: nil)
            }
            let albumAction = UIAlertAction(title: "Photo Album", style: .default) { (action) in
                self.imagePicker.sourceType = .photoLibrary
                self.imagePicker.allowsEditing = true
                
                self.present(self.imagePicker, animated: true, completion: nil)
            }
            let dismissAction = UIAlertAction(title: "Cancel", style: .cancel) { (action) in
                
                sender.tag = 1
                self.btnUpload.setTitle("Upload Options", for:.normal)
            }
            
            alertController.addAction(cameraAction)
            alertController.addAction(albumAction)
            alertController.addAction(dismissAction)
            present(alertController, animated: true, completion: nil)
        }
        else{
            
            do {
                let nameTitle = try getEnteredDocName()
                
                sender.tag = 1
                btnUpload.setTitle("Upload Options", for:.normal)
                
                let selectedImage = self.imgView.image
                let imageData = selectedImage!.jpegData(compressionQuality: 1.0)!
                                
                self.postReportImage(imageData: imageData, reportTitle: nameTitle)
                
            } catch {
                
                ServiceModel().handleError(error: error, parentView: self)
            }
            
        }
        
    }
    
    
    private func postReportImage(imageData: Data, reportTitle: String) {
        
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        let reportDate = formatter.string(from: Date())
        let reportTime = String(Int64(Date().timeIntervalSince1970))
        
        let parameters = ["user" : AppManager.shared.getPatientUsername(), "title": reportTitle, "creation_date": reportDate, "creation_time": reportTime, "type": uploadType]
        
        print(parameters)
        
        let headers: HTTPHeaders = [
            "x-appname" : AppManager.APP_NAME,
            "x-apptype" : "iOS(iPhone)",
            "x-debug" : "True",
            "x-usertype" : "PATIENT",
            "Authentication-Token" : UserDefaults.standard.string(forKey: "Authentcation_Token") ?? "No Token",
            "x-username" : AppManager.shared.getPatientUsername()
        ]
        
        guard let url = URL(string: AppManager.APP_BASE_URL + "/navia/investigations/") else {return}
        
        DispatchQueue.main.async {
            HUDIndicatorView.shared.showHUD(view: self.view,
                                            title: "Uploading ..")
        }
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in

            multipartFormData.append(imageData, withName: "uploaded_file", fileName: "\(reportTitle)-\(reportTime).jpg", mimeType: "image/jpeg")
            
            for (k,v) in parameters {
                multipartFormData.append(v.data(using: .utf8)!, withName: k)
            }
            
        }, to: url,method: .post,headers: headers) {[weak self] (result) in
            switch result{
                
            case .success(let request, _, _):
                request.responseJSON { response in
                    
                    print(response)
                    if let json = response.result.value as? NSDictionary {
                        DispatchQueue.main.async {
                            
                            print(json)
                            
                            HUDIndicatorView.shared.dismissHUD()
                            
                            if let status = json.value(forKey: "result") as? String {
                                
                                if(status == "SUCCESS"){
                                    
                                    Toast.show(message: "You have uploaded successfully.", controller: self!)
                                    
                                    DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
                                        self?.navigationController?.popViewController(animated: false)
                                    }
                                }
                                else{
                                    
                                    Toast.show(message: "Failed to upload, please try again.", controller: self!)
                                    
                                    self?.btnUpload.tag = 2
                                    self?.btnUpload.setTitle("Upload File", for:.normal)
                                }
                            }
                            
                        }
                    }
                }
                
            case .failure(let error):
                DispatchQueue.main.async {
                    
                    HUDIndicatorView.shared.dismissHUD()
                    print("Error in upload: \(error.localizedDescription)")
                    
                    Toast.show(message: "Failed to upload, please try again.", controller: self!)
                    
                    self?.btnUpload.tag = 2
                    self?.btnUpload.setTitle("Upload File", for:.normal)
                    
                }
                
            @unknown default:
                print("default")
            }
        }
    }
}

extension UploadReportViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        let mediaType = info[UIImagePickerController.InfoKey.mediaType] as! String
        if mediaType.isEqual(kUTTypeImage as String) {
            
            if let image = info[.editedImage] as? UIImage {
                upload(image: image)
            }
                
            else if let image = info[.originalImage] as? UIImage {
                upload(image: image)
                
            }
            
        }
        picker.dismiss(animated: true)
        
        
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    func upload(image: UIImage) {
        self.imgView.image = image
        
    }
}

extension UploadReportViewController : UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
