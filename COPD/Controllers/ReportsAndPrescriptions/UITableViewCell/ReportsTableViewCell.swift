//
//  ReportsTableViewCell.swift
//  Navia Health Manager
//
//  Created by Anand Prakash on 28/03/20.
//  Copyright © 2020 Anand Prakash. All rights reserved.
//

import UIKit

class ReportsTableViewCell: UITableViewCell {
    
    @IBOutlet weak var containerView: UIView!
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var lblType: UILabel!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
