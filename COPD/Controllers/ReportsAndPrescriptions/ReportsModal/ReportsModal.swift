//
//  ReportsViewController.swift
//  Navia Health Manager
//
//  Created by Anand Prakash on 28/03/20.
//  Copyright © 2020 Anand Prakash. All rights reserved.
//

import Foundation

// MARK: - ReportsModal
struct ReportsModal: Codable {
    let data: [Reports]?
    let result: String?
}

// MARK: - Reports
struct Reports: Codable {
    let status: Status?
    let title: String?
    let uploadType: UploadType?
    let creationTime: String?
    let createdBy: CreatedBy?
    let creationDate: String?
    let id, user: Int?
    let report: String?

    enum CodingKeys: String, CodingKey {
        case status, title
        case uploadType = "upload_type"
        case creationTime = "creation_time"
        case createdBy = "created_by"
        case creationDate = "creation_date"
        case id, user, report
    }
}

enum CreatedBy: String, Codable {
    case the8800777690 = "8800777690"
    case videoSample2 = "video_sample2"
}

enum Status: String, Codable {
    case created = "CREATED"
}

enum UploadType: String, Codable {
    case prescription = "Prescription"
    case report = "Report"
}
