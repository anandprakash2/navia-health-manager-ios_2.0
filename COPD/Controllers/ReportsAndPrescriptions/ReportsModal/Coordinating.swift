//
//  Coordinating.swift
//  Navia Health Manager
//
//  Created by Shivani Dosajh on 17/11/17.
//  Copyright © 2017 Navia Life Care. All rights reserved.
//

/*:
 ### Coordinator
 */

import UIKit
/*
 * @description : Coordinator Protocol for managing view controller transitions
 @var root :- reference to the root controller in the view hierarchy
 @var viewController :- reference to the current visible controller
 */

protocol Coordinating : class {
    associatedtype ViewController: UIViewController
    associatedtype RootViewController : UIViewController
    var viewController: ViewController? { get set }
    var root : RootViewController { get }
    func createViewController () -> ViewController
    func configure (vc : ViewController)
    func show (vc : ViewController)
    
}

/*
 * Methods responsible for view presentation
 */

extension Coordinating {
    
    func configure(vc: ViewController) {
    }
    func show(vc: ViewController) {
        print("---------> ROOT : \(root) ")
        print("\n---------> VC : \(vc)")
        print("-------------------\n")
        
        root.show(vc, sender: self)
        
    }
    func dismiss() {
        root.dismiss(animated: true, completion: nil)
    }
}
/*
 * Coordinator control methods
 */

extension Coordinating {
    func start() {
        let vc = createViewController()
        self.viewController = vc
        configure(vc: vc)
        show(vc: vc)
    }
    func stop() {
        dismiss()
        self.viewController = nil
    }
}
