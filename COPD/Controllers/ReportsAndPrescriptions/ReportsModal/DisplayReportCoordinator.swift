//
//  DisplayReportCoordinator.swift
//  Navia Health Manager
//
//  Created by Kapil Rathore on 05/12/17.
//  Copyright © 2017 Navia Life Care. All rights reserved.
//

import UIKit

class DisplayReportCoordinator : Coordinating {
    
    typealias ViewController = DisplayReportViewController
    
    var viewController: ViewController?
    var root: UIViewController
    var url = ""
    var type = ""
    
    func createViewController() -> ViewController {
        return ViewController(nibName: "DisplayReportViewController", bundle: nil)
    }
    
    func configure(vc: ViewController) {
        if let rootVc = root as? UINavigationController {
            rootVc.pushViewController(vc, animated: true)
        }
        vc.urlLink = url
        vc.docType = type
    }
    
    func show(vc: DisplayReportViewController) {
        
    }
    
    init(root : UIViewController, url: String, type: String) {
        self.root  = root
        self.url = url
        self.type = type
    }
}
