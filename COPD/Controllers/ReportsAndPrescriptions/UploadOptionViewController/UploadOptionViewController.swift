//
//  UploadOptionViewController.swift
//  Navia Health Manager
//
//  Created by Anand Prakash on 28/03/20.
//  Copyright © 2020 Anand Prakash. All rights reserved.
//

import UIKit

protocol OptionViewDelegate {
    func navigateToUploadNewFile(uploadType: String)
}

class UploadOptionViewController: UIViewController {
    
    var delegate: OptionViewDelegate?
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(didTapOutsideView)))
    }
    
    @objc private func didTapOutsideView(){
        self.dismiss(animated: false, completion: nil)
    }
    
    
    @IBAction func uploadReport(_ sender: Any) {
        self.dismiss(animated: false, completion: {
            self.delegate?.navigateToUploadNewFile(uploadType: "Report")
            
        })
        
    }
    
    @IBAction func uploadPrescription(_ sender: Any) {
        self.dismiss(animated: false, completion: {
            self.delegate?.navigateToUploadNewFile(uploadType: "Prescription")
        })
    }
    
}
