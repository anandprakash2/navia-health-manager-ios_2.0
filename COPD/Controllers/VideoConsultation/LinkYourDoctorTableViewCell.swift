//
//  LinkYourDoctorTableViewCell.swift
//  Navia Health Manager
//
//  Created by Sumit Mishra on 31/03/20.
//  Copyright © 2020 IOS Development - NAvia. All rights reserved.
//

import UIKit

protocol LinkYourDoctorDelegate {
    func linkDoctor()
}

class LinkYourDoctorTableViewCell: UITableViewCell {

    
    var delegate: LinkYourDoctorDelegate?
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    @IBAction func linkDoctorsView(_ sender: Any) {
        self.delegate?.linkDoctor()
    }
    
}
