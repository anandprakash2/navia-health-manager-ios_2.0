//
//  VideoCallHistoryViewController.swift
//  Navia Health Manager
//
//  Created by Sumit Mishra on 30/03/20.
//  Copyright © 2020 IOS Development - NAvia. All rights reserved.
//

import UIKit

class VideoCallHistoryViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        registerNib()
    }
    
    func registerNib(){
    
        let customNib = UINib(nibName: "VideoCallHistoryTableViewCell", bundle: Bundle.main)
        tableView.register(customNib, forCellReuseIdentifier: "VideoCallHistoryTableViewCell")
        tableView.separatorStyle = .none
        
    }
    @IBAction func requestVideoCall(_ sender: Any) {
        let storyBoard = UIStoryboard.init(name: "VideoCall", bundle: nil)
        let nextVC = storyBoard.instantiateViewController(withIdentifier: "LinkedDoctorsViewController") as! LinkedDoctorsViewController
        self.navigationController?.pushViewController(nextVC, animated: false)
    }
    
    @IBAction func backBtn(_ sender: Any) {
        self.navigationController?.popViewController(animated: false)
    }
    
}

extension VideoCallHistoryViewController: UITableViewDataSource, UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
            return 5
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
        
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
            let cell = tableView.dequeueReusableCell(withIdentifier: "VideoCallHistoryTableViewCell", for: indexPath) as! VideoCallHistoryTableViewCell
            
            return cell
        
        
    }
    
}
