//
//  ReviewAppointmentViewController.swift
//  Navia Health Manager
//
//  Created by Sumit Mishra on 31/03/20.
//  Copyright © 2020 IOS Development - NAvia. All rights reserved.
//

import UIKit

class ReviewAppointmentViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    @IBAction func backBtn(_ sender: Any) {
        self.navigationController?.popViewController(animated: false)
    }
    
    @IBAction func bookAppointment(_ sender: Any) {
        let nextVC = self.storyboard?.instantiateViewController(withIdentifier: "AppointmentBookedViewController") as! AppointmentBookedViewController
        nextVC.delegate = self
        nextVC.modalPresentationStyle = .overCurrentContext
        self.navigationController?.present(nextVC, animated: false, completion: nil)
    }
    
}

extension ReviewAppointmentViewController: AppointmentDelegate{
    func popView() {
        self.navigationController?.popToViewController(ofClass: VideoCallHistoryViewController.self)

    }
    
    
}
