//
//  LinkYourDoctorViewController.swift
//  Navia Health Manager
//
//  Created by Sumit Mishra on 31/03/20.
//  Copyright © 2020 IOS Development - NAvia. All rights reserved.
//

import UIKit
import DropDown

class LinkYourDoctorViewController: UIViewController {

    @IBOutlet weak var filterView: UIView!
    @IBOutlet weak var searchTxtFld: DesignableUITextField!
    @IBOutlet weak var tableView: UITableView!
    
    let menuDropDown = DropDown()

    override func viewDidLoad() {
        super.viewDidLoad()
        setUpTouchableView()
        registerNib()
    }

    func registerNib(){
    
        let customNib = UINib(nibName: "LinkYourDoctorTableViewCell", bundle: Bundle.main)
        tableView.register(customNib, forCellReuseIdentifier: "LinkYourDoctorTableViewCell")
        tableView.separatorStyle = .none
        
    }
    
    func setUpTouchableView(){
        self.filterView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(didTapSelectFilterView)))
    }
    
    @objc private func didTapSelectFilterView() {
        menuDropDown.anchorView = self.filterView
        menuDropDown.bottomOffset = CGPoint(x: 0, y: 45)
        menuDropDown.dataSource = ["Search by name","Search by specialization","Search by hospital","Search by city"]
        menuDropDown.selectionAction = { [weak self] (index, item) in
            self!.searchTxtFld.placeholder = item
        }
        menuDropDown.show()
    }
    
    @IBAction func backBtn(_ sender: Any) {
        self.navigationController?.popViewController(animated: false)
    }
    
}

extension LinkYourDoctorViewController: UITableViewDataSource, UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        let nextVC = self.storyboard?.instantiateViewController(withIdentifier: "DoctorsProfileViewController") as! DoctorsProfileViewController
        self.navigationController?.pushViewController(nextVC, animated: false)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
            return 5
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
        
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
            let cell = tableView.dequeueReusableCell(withIdentifier: "LinkYourDoctorTableViewCell", for: indexPath) as! LinkYourDoctorTableViewCell
        cell.delegate = self
            return cell
        
        
    }
    
}

extension LinkYourDoctorViewController: LinkYourDoctorDelegate{
    func linkDoctor() {
        let refreshAlert = UIAlertController(title: "Confirm Doctor Link", message: "Confirm Link with Doctor Balwant Singh", preferredStyle: UIAlertController.Style.alert)

        refreshAlert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (action: UIAlertAction!) in
            self.navigationController?.popViewController(animated: false)
        }))

        refreshAlert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (action: UIAlertAction!) in
            print("Handle Cancel Logic here")
        }))

        present(refreshAlert, animated: true, completion: nil)
    }
}
