//
//  AppointmentBookedViewController.swift
//  NaviaDoctors
//
//  Created by Sumit Mishra on 29/01/20.
//  Copyright © 2020 IOS Development - NAvia. All rights reserved.
//

import UIKit

protocol AppointmentDelegate {
    func popView()
}

class AppointmentBookedViewController: UIViewController {

    
    @IBOutlet weak var selectDate: UILabel!
    @IBOutlet weak var time: UILabel!
    @IBOutlet weak var patientName: UILabel!
    
    var selectedTime: String? = ""
    var patientNAME: String = ""
    var selectedDate: String? = ""
    
    var delegate: AppointmentDelegate?
    override func viewDidLoad() {
        super.viewDidLoad()
//        updateView()
    }

//    func updateView(){
//        patientName.text = patientNAME
//        selectDate.text = selectedDate
//        time.text = selectedTime
//    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func crossBtnClicked(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func viewAppointment(_ sender: Any) {
        self.dismiss(animated: true, completion: {
            self.delegate?.popView()
        })
    }
}

extension UINavigationController {
  func popToViewController(ofClass: AnyClass, animated: Bool = true) {
    if let vc = viewControllers.last(where: { $0.isKind(of: ofClass) }) {
      popToViewController(vc, animated: animated)
    }
  }
}
