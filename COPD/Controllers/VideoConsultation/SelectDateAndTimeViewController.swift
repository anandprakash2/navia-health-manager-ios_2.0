//
//  SelectDateAndTimeViewController.swift
//  Navia Health Manager
//
//  Created by Sumit Mishra on 30/03/20.
//  Copyright © 2020 IOS Development - NAvia. All rights reserved.
//

import UIKit
import DropDown

class SelectDateAndTimeViewController: UIViewController {
    
    @IBOutlet weak var selectedDateView: UIView!
    @IBOutlet weak var selectedDate: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var noSessionsLbl: UILabel!
    
    private var selectedIndex: Int!
    let menuDropDown = DropDown()
    var selectDate: [String] = []
    
    var timingArray: [String] = ["10:00","10:00","10:00","10:00","10:00","10:00","10:00"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        selectedDate.text = "SELECT DATE"
        getDates()
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
        layout.itemSize = CGSize(width: self.collectionView.bounds.width/4 - 20, height: 40)
        layout.minimumInteritemSpacing = 0
        layout.minimumLineSpacing = 0
        collectionView!.collectionViewLayout = layout
        registerNib()
    }
    
    
    
    
    func getDates(){
        let formatter        = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        let currDate         = formatter.date(from: Date().currentDate)
        for i in 0..<60
        {
            let newDate = Calendar.current.date(byAdding: .day, value: i, to: currDate!)
            let dateString = self.convertDateFormater("\(newDate!)")
            self.selectDate.append(dateString)
        }
        
        self.setUpTouchableView()
    }
    
    func setUpTouchableView(){
        self.selectedDateView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(didTapSelectDateView)))
        selectedDate.text = "SELECT DATE"
    }
    
    @objc private func didTapSelectDateView() {
        menuDropDown.anchorView = self.selectedDateView
        menuDropDown.bottomOffset = CGPoint(x: 0, y: 45)
        menuDropDown.dataSource = selectDate
        menuDropDown.selectionAction = { [weak self] (index, item) in
            self!.selectedDate.text = item
        }
        menuDropDown.show()
    }
    
    
    
    func convertDateFormater(_ date: String) -> String
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss z"
        let date = dateFormatter.date(from: date)
        dateFormatter.dateFormat = "yyyy-MM-dd"
        return  dateFormatter.string(from: date!)
        
    }
    
    
    func registerNib(){
        
        let nib = UINib(nibName: "SelectTimeCollectionViewCell", bundle: nil)
        collectionView?.register(nib, forCellWithReuseIdentifier: "SelectTimeCollectionViewCell")
        
    }
    
    @IBAction func backBtnClicked(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func submitClicked(_ sender: Any) {
        
        let nextVC = self.storyboard?.instantiateViewController(withIdentifier: "ReviewAppointmentViewController") as! ReviewAppointmentViewController
        self.navigationController?.pushViewController(nextVC, animated: false)
        
    }
}

extension SelectDateAndTimeViewController: UICollectionViewDelegate,UICollectionViewDataSource , UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        selectedIndex = indexPath.row
//        selectedTime = timingArray[indexPath.row]
        collectionView.reloadData()
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return timingArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SelectTimeCollectionViewCell", for: indexPath) as! SelectTimeCollectionViewCell
        cell.timeSlotLbl.text = timingArray[indexPath.row]
        if selectedIndex == indexPath.row{
            cell.timeSlotLbl.backgroundColor = UIColor.init(red: 61.0/255.0, green: 132.0/255.0, blue: 168.0/255.0, alpha: 1.0)
        }else{
            cell.timeSlotLbl.backgroundColor = .clear
        }
        return cell
    }
}

