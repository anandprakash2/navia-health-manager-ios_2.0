//
//  LinkedDoctorsTableViewCell.swift
//  Navia Health Manager
//
//  Created by Sumit Mishra on 30/03/20.
//  Copyright © 2020 IOS Development - NAvia. All rights reserved.
//

import UIKit

protocol BookVideoCallDelegate{
    func bookVideoCall()
}

class LinkedDoctorsTableViewCell: UITableViewCell {

    var delegate: BookVideoCallDelegate?
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    @IBAction func bookVideoCall(_ sender: Any) {
        delegate?.bookVideoCall()
    }
    
    
}
