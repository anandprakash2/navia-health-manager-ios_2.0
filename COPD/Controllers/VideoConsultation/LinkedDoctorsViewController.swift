//
//  LinkedDoctorsViewController.swift
//  Navia Health Manager
//
//  Created by Sumit Mishra on 30/03/20.
//  Copyright © 2020 IOS Development - NAvia. All rights reserved.
//

import UIKit

class LinkedDoctorsViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        registerNib()
    }
    
    
    func registerNib(){
    
        let customNib = UINib(nibName: "LinkedDoctorsTableViewCell", bundle: Bundle.main)
        tableView.register(customNib, forCellReuseIdentifier: "LinkedDoctorsTableViewCell")
        tableView.separatorStyle = .none
        
    }
    
    @IBAction func plusBtn(_ sender: Any) {
        let nextVC = self.storyboard?.instantiateViewController(withIdentifier: "LinkYourDoctorViewController") as! LinkYourDoctorViewController
        self.navigationController?.pushViewController(nextVC, animated: false)
    }
    
    @IBAction func backBtn(_ sender: Any) {
        self.navigationController?.popViewController(animated: false)
    }
    
}

extension LinkedDoctorsViewController: UITableViewDataSource, UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        let nextVC = self.storyboard?.instantiateViewController(withIdentifier: "DoctorsProfileViewController") as! DoctorsProfileViewController
        self.navigationController?.pushViewController(nextVC, animated: false)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
            return 5
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
        
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
            let cell = tableView.dequeueReusableCell(withIdentifier: "LinkedDoctorsTableViewCell", for: indexPath) as! LinkedDoctorsTableViewCell
        cell.delegate = self
            return cell
        
        
    }
    
}

extension LinkedDoctorsViewController: BookVideoCallDelegate{
    func bookVideoCall() {
        let storyBoard = UIStoryboard.init(name: "VideoCall", bundle: nil)
        let nextVC = storyBoard.instantiateViewController(withIdentifier: "SelectDateAndTimeViewController") as! SelectDateAndTimeViewController
        self.navigationController?.pushViewController(nextVC, animated: false)
    }
}
