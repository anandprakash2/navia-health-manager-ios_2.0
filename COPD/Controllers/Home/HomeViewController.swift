//
//  HomeViewController.swift
//  COPD
//
//  Created by Anand Prakash on 17/10/19.
//  Copyright © 2019 Anand Prakash. All rights reserved.
//

import Foundation
import UIKit

class HomeViewController: UIViewController {
    
    // MARK: - Declarations
    
    
    // MARK: - Lifecycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        

        
    }

    
    override func viewWillAppear(_ animated: Bool) {
           super.viewWillAppear(animated)
        
        // Set the Status bar bg color for iOS 13
        if #available(iOS 13.0, *) {
            let app = UIApplication.shared
            let statusBarHeight: CGFloat = app.statusBarFrame.size.height
            let statusbarView = UIView()
            statusbarView.backgroundColor = #colorLiteral(red: 0.1490196078, green: 0.4588235294, blue: 0.8196078431, alpha: 1)
            view.addSubview(statusbarView)
            statusbarView.translatesAutoresizingMaskIntoConstraints = false
            statusbarView.heightAnchor
                .constraint(equalToConstant: statusBarHeight).isActive = true
            statusbarView.widthAnchor
                .constraint(equalTo: view.widthAnchor, multiplier: 1.0).isActive = true
            statusbarView.topAnchor
                .constraint(equalTo: view.topAnchor).isActive = true
            statusbarView.centerXAnchor
                .constraint(equalTo: view.centerXAnchor).isActive = true
        } else {// For other iOS
            let statusBar = UIApplication.shared.value(forKeyPath: "statusBarWindow.statusBar") as? UIView
            statusBar?.backgroundColor = #colorLiteral(red: 0.1490196078, green: 0.4588235294, blue: 0.8196078431, alpha: 1)
        }
        
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        // Progress HUD
        /*HUDIndicatorView.shared.showHUD(view: self.view,
         title: "App Loading")
         
         HUDIndicatorView.shared.showHUD(view: self.view)
         HUDIndicatorView.shared.dismissHUD()
         
         // Toast
         Toast.show(message: "Allow Location permission to access your current location.", controller: self)
         
         // Alert View
         ServiceModel().showAlert(title: AppManager.APP_NAME, message: "Invalid email id", parentView: self)
         
         // Displaying color from Hex code
         lblText.textColor = ColorConverter.hexStringToUIColor(hex: "#ff9a00")
         
         // Displaying color directly
         lblText.textColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
         */
        
    }
    
    
    // MARK: - IBActions
    @IBAction func myAction(_ sender: UIButton) {
        
        print("navigate")
        
    }
    
}



