//
//  SelectDOBViewController.swift
//  NaviaDoctors
//
//  Created by Sumit Mishra on 14/02/20.
//  Copyright © 2020 IOS Development - NAvia. All rights reserved.
//

import UIKit

protocol DateOFBirthDelegate {
    func selectDOB(dob: String)
}

class SelectDOBViewController: UIViewController {
    
    var delegate: DateOFBirthDelegate?
    @IBOutlet weak var pickerView: UIDatePicker!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(didTapOutsideView)))
    }
    
    @objc private func didTapOutsideView(){
        self.dismiss(animated: false, completion: nil)
    }
    
    @IBAction func saveBtnClicked(_ sender: Any) {
        self.dismiss(animated: false, completion: {
            let selectedDate = self.convertDateFormater("\(self.pickerView.date)")
            self.delegate?.selectDOB(dob: selectedDate)
        })
    }
    
    func convertDateFormater(_ date: String) -> String
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss z"
        let date = dateFormatter.date(from: date)
        dateFormatter.dateFormat = "yyyy-MM-dd"
        return  dateFormatter.string(from: date!)

    }
}
