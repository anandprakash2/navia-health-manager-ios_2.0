//
//  ProfileViewController.swift
//  Navia Health Manager
//
//  Created by Sumit Mishra on 27/03/20.
//  Copyright © 2020 IOS Development - NAvia. All rights reserved.
//

import UIKit

class ProfileViewController: UIViewController {

    @IBOutlet weak var userView: UIView!
    @IBOutlet weak var medicalView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpTouchableView()
        
    }
    
    func setUpTouchableView(){
        self.userView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(didTapUserView)))
        self.medicalView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(didTapMedicalView)))

    }
    
    @objc private func didTapUserView() {
        let nextVC = self.storyboard?.instantiateViewController(withIdentifier: "UserProfileViewController") as! UserProfileViewController
        self.navigationController?.pushViewController(nextVC, animated: false)
    }
    
    @objc private func didTapMedicalView() {
        print("Medical View")
    }
    
    @IBAction func backBtn(_ sender: Any) {
        self.navigationController?.popViewController(animated: false)
    }
}
