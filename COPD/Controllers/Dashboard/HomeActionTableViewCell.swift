//
//  HomeActionTableViewCell.swift
//  Dashboard
//
//  Created by Sumit Mishra on 26/03/20.
//  Copyright © 2020 Sumit Mishra. All rights reserved.
//

import UIKit

class HomeActionTableViewCell: UITableViewCell {

    @IBOutlet weak var mainImage: UIImageView!
    @IBOutlet weak var subtitleLbl: UILabel!
    @IBOutlet weak var titleLbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
}
