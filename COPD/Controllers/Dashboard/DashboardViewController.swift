//
//  DashboardViewController.swift
//  Dashboard
//
//  Created by Sumit Mishra on 26/03/20.
//  Copyright © 2020 Sumit Mishra. All rights reserved.
//

import UIKit

class DashboardViewController: UIViewController {

    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var patientGender: UILabel!
    @IBOutlet weak var patientName: UILabel!
    @IBOutlet weak var feedTableView: UITableView!
    @IBOutlet weak var feedBtn: UIButton!
    @IBOutlet weak var homeBtn: UIButton!
    @IBOutlet weak var feedView: UIView!
    @IBOutlet weak var homeView: UIView!
    var titleArray: [String] = ["BOOK APPOINTMENT","VIDEO CONSULTATION","TREATMENTS","REPORTS and PRESCRIPTION"]
    var subTitleArray: [String] = ["Book appointment and rate your visits","Connect with doctor through video call","View treatments prescribed by doctors","Upload and view reports and prescriptions uploaded by doctors"]
    var sideMenuNavigationController: SideMenuNavigationController?
    var isSideMenuPresneted = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        feedTableView.tag = 1
        fetchUserData()
        selectHomePage()
        setupSideMenu()
        registerNib()
    }
    
    func selectHomePage(){
        homeView.isHidden = false
        homeBtn.backgroundColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        feedBtn.backgroundColor = .clear
        feedView.isHidden = true
    }
    
    func selectFeedPage(){
        homeView.isHidden = true
        homeBtn.backgroundColor = .clear
        feedBtn.backgroundColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        feedView.isHidden = false
    }
    
    func registerNib(){
        
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        layout.scrollDirection = .horizontal
        layout.itemSize = CGSize(width: 160, height: self.collectionView.bounds.height)
        layout.minimumLineSpacing = 20
        collectionView!.collectionViewLayout = layout
        let nib = UINib(nibName: "AddVitalsCollectionViewCell", bundle: nil)
        collectionView?.register(nib, forCellWithReuseIdentifier: "AddVitalsCollectionViewCell")
                
        tableView.isScrollEnabled = false
        let customNib = UINib(nibName: "HomeActionTableViewCell", bundle: Bundle.main)
        tableView.register(customNib, forCellReuseIdentifier: "HomeActionTableViewCell")
        
        feedTableView.separatorStyle = .none
        let feedsTableViewCell = UINib(nibName: "FeedsTableViewCell", bundle: Bundle.main)
        feedTableView.register(feedsTableViewCell, forCellReuseIdentifier: "FeedsTableViewCell")
    }
    
    
    private func makeSettings() -> SideMenuSettings {
      let presentationStyle = SideMenuPresentationStyle.menuSlideIn
      presentationStyle.backgroundColor = .clear
      presentationStyle.menuStartAlpha = CGFloat(0.0)
      //   presentationStyle.menuScaleFactor = CGFloat(menuScaleFactorSlider.value)
      //   presentationStyle.onTopShadowOpacity = shadowOpacitySlider.value
      presentationStyle.presentingEndAlpha = CGFloat(0.6)
      //   presentationStyle.presentingScaleFactor = CGFloat(presentingScaleFactorSlider.value)

      var settings = SideMenuSettings()
      settings.presentationStyle = presentationStyle
      // settings.menuWidth = min(view.frame.width, view.frame.height) * CGFloat(screenWidthSlider.value)
      let styles:[UIBlurEffect.Style?] = [nil, .dark, .light, .extraLight]
      settings.blurEffectStyle = nil
      settings.statusBarEndAlpha = 1
      return settings
    }
    
    private func setupSideMenu() {
      // Define the menus
        
//        let settings = makeSettings()
//        SideMenuManager.default.leftMenuNavigationController?.settings = settings
//        if viewControllers.count > 0 {
//          SideMenuManager.default.addScreenEdgePanGesturesToPresent(toView: viewControllers[0].view)
//        }
        
      sideMenuNavigationController = storyboard?.instantiateViewController(withIdentifier: "LeftMenuNavigationController") as? SideMenuNavigationController
      let leftController = sideMenuNavigationController?.viewControllers.first as? LeftViewController
      sideMenuNavigationController?.sideMenuDelegate = self
      
      leftController?.delegate = self
      SideMenuManager.default.leftMenuNavigationController = sideMenuNavigationController
      
      // Enable gestures. The left and/or right menus must be set up above for these to work.
      // Note that these continue to work on the Navigation Controller independent of the View Controller it displays!
      SideMenuManager.default.addPanGestureToPresent(toView: navigationController!.navigationBar)
      SideMenuManager.default.addScreenEdgePanGesturesToPresent(toView: view)
    }
    
    @IBAction func leftMenu(_ sender: Any) {
        if !self.isSideMenuPresneted {
               self.navigationController?.present(self.sideMenuNavigationController!, animated: true, completion: nil)
             } else {
               self.sideMenuNavigationController?.dismiss(animated: true, completion: nil)
             }
             
             isSideMenuPresneted = !isSideMenuPresneted
    }
    
    
        // Get Reports data
        func fetchUserData(){
            
            let servicemodel = ServiceModel()
            
            if (NetworkConnection.isConnectedToNetwork()){
                
                do {
                    
                    HUDIndicatorView.shared.showHUD(view: self.view,
                                                    title: "Loading ..")
                    
                    DispatchQueue.global(qos: .userInitiated).async {
                        
                        servicemodel.fetchUserProfile(username: AppManager.shared.getPatientUsername()) { responseStr in
                            print(responseStr!)
                            
                            DispatchQueue.main.async {
                                
                                if let status = responseStr?.result {
                                    
                                    if(status == "SUCCESS"){
                                        
    //                                    self.reportsArray = responseStr!.data!
                                        let data = responseStr?.data
                                        
                                        self.patientName.text = "Hi , " + "\(data!.fields.user.firstName)" + "\(data!.fields.user.lastName)"
                                        
                                        self.patientGender.text = (data!.fields.gender).uppercased()
                                        
                                        HUDIndicatorView.shared.dismissHUD()
                                        
                                    }
                                    else{
                                        
                                        HUDIndicatorView.shared.dismissHUD()
                                        
                                        // handle Error or show error message to the user
                                        servicemodel.showAlert(title: status, message: "Failed to load Reports.", parentView: self)
                                    }
                                    
                                }
                                
                            }
                        }
                    }
                    
                }
            }
        }
 
    
    
    @IBAction func homeBtnClicked(_ sender: Any) {
        selectHomePage()
    }
    
    
    @IBAction func feedBtnClicked(_ sender: Any) {
        selectFeedPage()
    }
    
}


extension DashboardViewController: UICollectionViewDelegate,UICollectionViewDataSource , UICollectionViewDelegateFlowLayout{

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {

    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 5
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AddVitalsCollectionViewCell", for: indexPath) as! AddVitalsCollectionViewCell
//        cell.titleLbl.text = suggestedArray![indexPath.row].medicine
        return cell
    }
}


//MARK:- Medicine Detail table View data source and delegate methods

extension DashboardViewController: UITableViewDataSource, UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: false)
        
        if indexPath.row == 1{
            let storyBoard = UIStoryboard.init(name: "VideoCall", bundle: nil)
            let nextVC = storyBoard.instantiateViewController(withIdentifier: "VideoCallHistoryViewController") as! VideoCallHistoryViewController
            self.navigationController?.pushViewController(nextVC, animated: false)
        }else if indexPath.row == 3{
            let nextVC = self.storyboard?.instantiateViewController(withIdentifier: "ReportsViewController") as! ReportsViewController
            self.navigationController?.pushViewController(nextVC, animated: false)
        }
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView.tag == 0{
            return titleArray.count
        }else{
            return 5
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
        
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView.tag == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: "HomeActionTableViewCell", for: indexPath) as! HomeActionTableViewCell
            cell.titleLbl.text = titleArray[indexPath.row]
            cell.subtitleLbl.text = subTitleArray[indexPath.row]
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "FeedsTableViewCell", for: indexPath) as! FeedsTableViewCell
            
            return cell
        }
        
    }
    
}


extension DashboardViewController: LeftMenuProtocol {

  func profileClicked() {
    //MOve to login Signup
    self.sideMenuNavigationController?.dismiss(animated: true, completion: {
        //
//      let profileController = Constants.StoryBoard.profile.instantiateViewController(withIdentifier: Constants.Segue.profile)
//      self.navigationController?.pushViewController(profileController, animated: true)
    })
  }

  func changeViewController(_ menu: IndexPath) {
    isSideMenuPresneted = !isSideMenuPresneted
    print("hello")
//    let webController = Constants.StoryBoard.dashboard.instantiateViewController(withIdentifier: Constants.Segue.webView)
//    self.navigationController?.present(webController, animated: true, completion: nil)
  }

  func loginSignUpClicked() {
    //MOve to login Signup
    self.sideMenuNavigationController?.dismiss(animated: true, completion: {
//      let loginNavigationController = Constants.StoryBoard.Login.instantiateViewController(withIdentifier: Constants.Segue.loginNavigation)
//      Utilities.getAppDelegate().window?.rootViewController = loginNavigationController
    })
  }
}

extension DashboardViewController: SideMenuNavigationControllerDelegate {
  
  func sideMenuWillAppear(menu: SideMenuNavigationController, animated: Bool) {
    print("SideMenu Appearing! (animated: \(animated))")
  }
  
  func sideMenuDidAppear(menu: SideMenuNavigationController, animated: Bool) {
    print("SideMenu Appeared! (animated: \(animated))")
    self.isSideMenuPresneted = true
  }
  
  func sideMenuWillDisappear(menu: SideMenuNavigationController, animated: Bool) {
    print("SideMenu Disappearing! (animated: \(animated))")
  }
  
  func sideMenuDidDisappear(menu: SideMenuNavigationController, animated: Bool) {
    print("SideMenu Disappeared! (animated: \(animated))")
    self.isSideMenuPresneted = false
  }
}
