//
//  SelectGenderViewController.swift
//  NaviaDoctors
//
//  Created by Sumit Mishra on 14/02/20.
//  Copyright © 2020 IOS Development - NAvia. All rights reserved.
//

import UIKit

protocol GenderDelegate {
    func selectGender(gender: String)
}

class SelectGenderViewController: UIViewController {

    var delegate: GenderDelegate?
    
    @IBOutlet weak var other: UIButton!
    @IBOutlet weak var female: UIButton!
    @IBOutlet weak var male: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(didTapOutsideView)))
    }
    
    @objc private func didTapOutsideView(){
        self.dismiss(animated: false, completion: nil)
    }
    
    @IBAction func saveBtnClicked(_ sender: Any) {
        switch (sender as AnyObject).tag {
        case 0:
            self.dismiss(animated: false, completion: {
                self.delegate?.selectGender(gender: "Male")
            })
            break
        case 1:
            self.dismiss(animated: false, completion: {
                self.delegate?.selectGender(gender: "Female")
            })
            break
        case 2:
            self.dismiss(animated: false, completion: {
                self.delegate?.selectGender(gender: "Other")
            })
            break
        default:
            break
        }
    }
}
