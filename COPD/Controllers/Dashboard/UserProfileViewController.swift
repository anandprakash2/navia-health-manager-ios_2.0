//
//  UserProfileViewController.swift
//  Navia Health Manager
//
//  Created by Sumit Mishra on 27/03/20.
//  Copyright © 2020 IOS Development - NAvia. All rights reserved.
//

import UIKit

class UserProfileViewController: UIViewController {

    @IBOutlet weak var mobileLbl: UILabel!
    @IBOutlet weak var nameTxtFld: UITextField!
    @IBOutlet weak var genderTxtFld: UITextField!
    @IBOutlet weak var dateTxtFld: UITextField!
    @IBOutlet weak var editBtn: UIButton!
    @IBOutlet weak var genderLbl: UILabel!
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var dateLbl: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        addTouchEvent()
        fetchUserData()
        dateLbl.isHidden = false
        genderLbl.isHidden = false
        nameLbl.isHidden = false
        nameTxtFld.isHidden = true
        genderTxtFld.isHidden = true
        dateTxtFld.isHidden = true
        editBtn.setTitle("EDIT", for: .normal)
        
        
    }
    
    
    // Get Reports data
    func fetchUserData(){
        
        let servicemodel = ServiceModel()
        
        if (NetworkConnection.isConnectedToNetwork()){
            
            do {
                
                HUDIndicatorView.shared.showHUD(view: self.view,
                                                title: "Loading ..")
                
                DispatchQueue.global(qos: .userInitiated).async {
                    
                    servicemodel.fetchUserProfile(username: AppManager.shared.getPatientUsername()) { responseStr in
                        print(responseStr!)
                        
                        DispatchQueue.main.async {
                            
                            if let status = responseStr?.result {
                                
                                if(status == "SUCCESS"){
                                    
//                                    self.reportsArray = responseStr!.data!
                                    let data = responseStr?.data
                                    
                                    self.nameLbl.text = "\(data!.fields.user.firstName)" + "\(data!.fields.user.lastName)"
                                    self.dateLbl.text = data?.fields.dateOfBirth
                                    self.genderLbl.text = data?.fields.gender
                                    self.mobileLbl.text = data?.fields.user.username
                                    HUDIndicatorView.shared.dismissHUD()
                                    
                                }
                                else{
                                    
                                    HUDIndicatorView.shared.dismissHUD()
                                    
                                    // handle Error or show error message to the user
                                    servicemodel.showAlert(title: status, message: "Failed to load Reports.", parentView: self)
                                }
                                
                            }
                            
                        }
                    }
                }
                
            }
        }
    }
    
    
    func editUserProfile(username:String, name:String, dob:String, gender:String){
        
        let servicemodel = ServiceModel()
        
        if (NetworkConnection.isConnectedToNetwork()){
            
            do {
                
                HUDIndicatorView.shared.showHUD(view: self.view,
                                                title: "Editing ..")
                
                DispatchQueue.global(qos: .userInitiated).async {
                    
                    servicemodel.editUserProfile(username: username, name: name, dob: dob, gender: gender) { responseStr in
                        print(responseStr)
                        
                        DispatchQueue.main.async {
                            
                            let status = responseStr.value(forKey: "result") as? String
                            
                            if(status == "SUCCESS"){
                                
                                // Save mobile number as phone number
//                                self.loginAction(phone: self.phoneNumber, pin: self.pin)
                                servicemodel.showAlert(title: "Success", message:"Profile Editted successfully.", parentView: self)

                                HUDIndicatorView.shared.dismissHUD()

                            }
                            else{
                                
                                let message = responseStr.value(forKey: "error_reason") as? String
                                servicemodel.showAlert(title: "Failed to register", message: message!, parentView: self)
                                
                                HUDIndicatorView.shared.dismissHUD()
                            }
                        }
                    }
                }
                
            } catch {
                
                servicemodel.handleError(error: error, parentView: self)
            }
        }else{
            servicemodel.showAlert(title: "Error", message:"Check Internet Connection", parentView: self)
        }
    }
    
    
    @IBAction func backBtnClicked(_ sender: Any) {
        self.navigationController?.popViewController(animated: false)
    }
    
    func addTouchEvent(){
        genderTxtFld.addTarget(self, action: #selector(genderSelect), for: UIControl.Event.editingDidBegin)
        dateTxtFld.addTarget(self, action: #selector(selectDob), for: UIControl.Event.editingDidBegin)
    }
    
    func resignRespoder(){
        nameTxtFld.resignFirstResponder()
    }
    
    @objc func genderSelect(textField: UITextField){
        
        resignRespoder()
                
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.01) {
            
            let nextVC = self.storyboard?.instantiateViewController(withIdentifier: "SelectGenderViewController") as! SelectGenderViewController
            nextVC.modalPresentationStyle = .overCurrentContext
            nextVC.delegate = self
            self.genderTxtFld.resignFirstResponder()
            self.navigationController?.present(nextVC, animated: false, completion: nil)
        }
        
    }
    
    @objc func selectDob(textField: UITextField){
        
        resignRespoder()
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.01) {
            
        let nextVC = self.storyboard?.instantiateViewController(withIdentifier: "SelectDOBViewController") as! SelectDOBViewController
        nextVC.modalPresentationStyle = .overCurrentContext
        nextVC.delegate = self
        self.dateTxtFld.resignFirstResponder()
        self.navigationController?.present(nextVC, animated: false, completion: nil)
        }
    }
    
    
    
    
    
    @IBAction func editBtnClicked(_ sender: Any) {
        if editBtn.titleLabel?.text == "EDIT"{
            dateLbl.isHidden = true
            genderLbl.isHidden = true
            nameLbl.isHidden = true
            nameTxtFld.isHidden = false
            genderTxtFld.isHidden = false
            dateTxtFld.isHidden = false
            
            editBtn.setTitle("SAVE", for: .normal)
        }else{
            
            
            editUserProfile(username: AppManager.shared.getPatientUsername(), name: nameTxtFld.text!, dob: dateTxtFld.text!, gender: genderTxtFld.text!)
            
            dateLbl.text = dateTxtFld.text
            nameLbl.text = nameTxtFld.text
            genderLbl.text = genderTxtFld.text
            dateLbl.isHidden = false
            genderLbl.isHidden = false
            nameLbl.isHidden = false
            nameTxtFld.isHidden = true
            genderTxtFld.isHidden = true
            dateTxtFld.isHidden = true
            editBtn.setTitle("EDIT", for: .normal)

        }
    }
    
}


extension UserProfileViewController: GenderDelegate{
    func selectGender(gender: String) {
        self.genderTxtFld.text = gender
        
    }
}

extension UserProfileViewController: DateOFBirthDelegate{
    func selectDOB(dob: String) {
        self.dateTxtFld.text = dob
    }
}

