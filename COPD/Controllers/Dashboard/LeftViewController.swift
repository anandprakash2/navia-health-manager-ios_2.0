//
//  LeftViewController.swift
//  SlideMenuControllerSwift

import UIKit

enum LeftMenu: Int {
    case main = 0
    case swift
    case java
    case go
    case nonMenu
}

protocol LeftMenuProtocol : class {
  func changeViewController(_ menu: IndexPath)
  func loginSignUpClicked()
  func profileClicked()
}

class LeftViewController : UITableViewController {

  @IBOutlet weak var loginSignupBtn: UIButton!
  @IBOutlet weak var profileBtn: UIButton!
  var menus = ["Medical History", "Family Members", "Body Vitals","Upcoming activities","Broadcasts","Logout"]

  var images = ["menu", "menu", "menu","menu","menu","menu"]
  var delegate: LeftMenuProtocol?

  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
  }

  override func viewDidLoad() {
    super.viewDidLoad()

//    if Utilities.getAccessToken() != nil {
//      self.loginSignupBtn.isUserInteractionEnabled = false
//      let name = UserDefaults.standard.object(forKey: Constants.UserDefault.name) as? String ?? " Logged In - Name not available"
//      self.loginSignupBtn.setTitle(name, for: .normal)
//    }

    profileBtn.layer.borderWidth = 0.5
    profileBtn.layer.masksToBounds = false
    profileBtn.layer.borderColor  = UIColor.lightGray.cgColor
    profileBtn.layer.cornerRadius = profileBtn.frame.size.width / 2
    profileBtn.clipsToBounds = true
    self.tableView.separatorColor = .none
    tableView.separatorColor = UIColor.clear
  }

  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
  }

  @IBAction func profileBtnClicked(_ sender: Any) {
    delegate?.profileClicked()
  }

  @IBAction func loginsignupBtnClicked(_ sender: UIButton) {
    delegate?.loginSignUpClicked()
  }

  func changeViewController(_ menu: LeftMenu) {
//        switch menu {
//        case .main:
//            self.slideMenuController()?.changeMainViewController(self.mainViewController, close: true)
//        case .swift:
//            self.slideMenuController()?.changeMainViewController(self.swiftViewController, close: true)
//        case .java:
//            self.slideMenuController()?.changeMainViewController(self.javaViewController, close: true)
//        case .go:
//            self.slideMenuController()?.changeMainViewController(self.goViewController, close: true)
//        case .nonMenu:
//            self.slideMenuController()?.changeMainViewController(self.nonMenuViewController, close: true)
//        }
   }
}

extension LeftViewController{
  override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
      return 44
    }

  override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    
    self.dismiss(animated: true) {
      self.delegate?.changeViewController(indexPath)
    }
 
    if let menu = LeftMenu(rawValue: indexPath.row) {
           // self.changeViewController(menu)
    }
  }

//  override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
//    if section == 0 {
//      return nil
//    }
//    let title = UILabel()
//    title.frame =  CGRect(x: 16, y: 8, width: self.view.frame.size.width - 20, height: 24) //width equals to parent view with 10 left and right marfrgin
//    title.text = self.section[section]
//    title.font = UIFont.appBoldFontWith(size: 16)
//
//    title.textColor = UIColor.hexStringToUIColor(hex: "#414141")
//    let headerView:UIView = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 24))
//    headerView.backgroundColor = UIColor.init(red: 242.0/255.0, green: 248.0/255.0, blue: 252.0/255.0, alpha: 1.0)
//    headerView.addSubview(title)
//    return headerView
//  }

//  override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
//    if section == 0 {
//      return 1
//    }
//    return 40
//  }

  override func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {

    if section == (menus.count - 1) {
     return 20
    }
    return 1
  }

  override func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
    
    let footerView:UIView = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 1))
    footerView.backgroundColor = UIColor.init(red: 242.0/255.0, green: 248.0/255.0, blue: 252.0/255.0, alpha: 1.0)

    let view = UIView(frame: CGRect(x: 16, y: 0, width: self.view.bounds.width - 40, height: 1))
    view.backgroundColor = UIColor.white
    footerView.addSubview(view)

    if section == (menus.count - 1) {
      view.backgroundColor = UIColor.init(red: 242.0/255.0, green: 248.0/255.0, blue: 252.0/255.0, alpha: 1.0)
    }
     return footerView
  }

}

extension LeftViewController {

  override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menus.count
    }

//  override func numberOfSections(in tableView: UITableView) -> Int {
//    return menus.count
//  }

  override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

    let cell = tableView.dequeueReusableCell(withIdentifier: "LeftCell", for: indexPath)
    let data = menus[indexPath.row]

    cell.textLabel?.text = data

    
      cell.imageView?.image = UIImage(named: images[indexPath.row])
      cell.imageView?.setImageColor(color: #colorLiteral(red: 0.2946019769, green: 0.5896945596, blue: 0.717130661, alpha: 1))
//      cell.textLabel?.font = UIFont.appSemiBoldFontWith(size: 15)
      cell.textLabel?.textColor = #colorLiteral(red: 0.2946019769, green: 0.5896945596, blue: 0.717130661, alpha: 1)
    
    return cell
  }

}

extension UIImageView {
    
  func setImageColor(color: UIColor) {
    let templateImage = self.image?.withRenderingMode(.alwaysTemplate)
    self.image = templateImage
    self.tintColor = color
  }
}

