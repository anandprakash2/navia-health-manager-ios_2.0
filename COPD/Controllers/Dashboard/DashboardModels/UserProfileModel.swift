//
//  UserProfileModel.swift
//  Navia Health Manager
//
//  Created by Sumit Mishra on 01/04/20.
//  Copyright © 2020 IOS Development - NAvia. All rights reserved.
//

import UIKit

// MARK: - UserDataModel
struct UserDataModel: Codable {
    let data: ProfileData
    let result: String
}

// MARK: - DataClass
struct ProfileData: Codable {
    let fields: Fields
}

// MARK: - Fields
struct Fields: Codable {
    let uhid, appName: String?
    let linkedMembers: [String]
    let image: String
    let profileID: Int
    let alternatePhone: String
    let parseAPIURL: String?
    let sendMessage: Int
    let dateOfBirthNew: String?
    let doctorsLinked, message: String
    let emailAddress, city: String?
    let introMesssage: Bool
    let userID: Int
    let activities: String
    let appVersion: String?
    let ageYear: String
    let state: String?
    let dateOfBirth: String
    let familyMembers: [Int]
    let facebookID: String?
    let deviceID, ageMonth: String
    let dueDate, pinCode, trialPeriodEndDate: String?
    let patientLocation, phone, associatedPartners: String
    let user: User
    let isOnline, watsappCheck: Bool
    let usingApps, subscription, subscriptionDate, naviaID: String
    let gender: String
    let age: String?
    let uhids, addressLine, notes: String

    enum CodingKeys: String, CodingKey {
        case uhid, appName
        case linkedMembers = "linked_members"
        case image
        case profileID = "profile_id"
        case alternatePhone = "alternate_phone"
        case parseAPIURL = "parse_api_url"
        case sendMessage = "send_message"
        case dateOfBirthNew = "date_of_birth_new"
        case doctorsLinked = "doctors_linked"
        case message
        case emailAddress = "email_address"
        case city
        case introMesssage = "intro_messsage"
        case userID = "user_id"
        case activities, appVersion
        case ageYear = "age_year"
        case state
        case dateOfBirth = "date_of_birth"
        case familyMembers = "family_members"
        case facebookID = "facebook_id"
        case deviceID = "device_id"
        case ageMonth = "age_month"
        case dueDate = "due_date"
        case pinCode = "pin_code"
        case trialPeriodEndDate = "trial_period_end_date"
        case patientLocation = "patient_location"
        case phone
        case associatedPartners = "associated_partners"
        case user
        case isOnline = "is_online"
        case watsappCheck = "watsapp_check"
        case usingApps = "using_apps"
        case subscription
        case subscriptionDate = "subscription_date"
        case naviaID = "navia_id"
        case gender, age, uhids
        case addressLine = "address_line"
        case notes
    }
}

// MARK: - User
struct User: Codable {
    let username, firstName, lastName: String
    let isActive, isSuperuser, isStaff: Bool
    let lastLogin: String?
    let groups, userPermissions: [String]
    let password, email, dateJoined: String

    enum CodingKeys: String, CodingKey {
        case username
        case firstName = "first_name"
        case lastName = "last_name"
        case isActive = "is_active"
        case isSuperuser = "is_superuser"
        case isStaff = "is_staff"
        case lastLogin = "last_login"
        case groups
        case userPermissions = "user_permissions"
        case password, email
        case dateJoined = "date_joined"
    }
}
