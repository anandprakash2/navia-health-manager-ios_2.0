//
//  ForgotPasswordVC.swift
//  COPD
//
//  Created by Anand Prakash on 23/10/19.
//  Copyright © 2019 Anand Prakash. All rights reserved.
//

import Foundation
import UIKit
import CoreLocation

class ForgotPasswordVC: UIViewController {
    
    // MARK: - Declarations
    @IBOutlet weak var phoneTextField: UITextField!
    @IBOutlet weak var pinTextField: UITextField!
    @IBOutlet weak var confirmPinTextField: UITextField!
        
    @IBOutlet weak var btnConfirmOTP: UIButton!
    @IBOutlet var otpPopupView: UIView!
    
    var otpView: PHOTPView!
    var enteredOtp: String = ""
    var receivedOtp: String? = ""
    var config:PinConfig! = PinConfig()
        
    // MARK: - Lifecycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Hide OTP popup view
        self.otpPopupView.frame = self.view.frame
        self.view.addSubview(self.otpPopupView)
        self.otpPopupView.isHidden = true
        
        // Save loginType to Userdefaults
        UserDefaults.standard.set("patient", forKey: "Logintype")
        UserDefaults.standard.synchronize()
        
        setUpOTPUI()
        
        self.disbleConfirmButton()
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        
    }

    
    func getEnteredPhoneNumber() throws -> String {
        
        guard let phoneNumber = phoneTextField.text,
            phoneNumber != "" else {
                throw AppError.LoginScreenError.phoneFieldEmpty
        }
        
        if !Utility.isValidPhoneNumber(phoneNumber: phoneNumber) {
            throw AppError.LoginScreenError.invalidPhoneNumber
        }
        
        return phoneNumber
    }
    
    func getEnteredPin() throws -> String {
        
        guard let pin = pinTextField.text,
            pin != "" else {
                throw AppError.LoginScreenError.newPinFieldEmpty
        }
        
        if pin.count < 6{
            throw AppError.LoginScreenError.invalidPin
        }
        
        return pin
    }
    
    func getEnteredConfirmPin() throws -> String {
        
        guard let confirmPin = confirmPinTextField.text,
            confirmPin != "" else {
                throw AppError.LoginScreenError.confirmPinFieldEmpty
        }
        
        return confirmPin
    }
    
    
    // MARK: - IBActions
    
    @IBAction func cancelAction(_ sender: UIButton) {
        
        self.otpPopupView.isHidden = true
    }
    
    @IBAction func confirmAction(_ sender: UIButton) {
        
        self.otpPopupView.isHidden = true
        
        let servicemodel = ServiceModel()
        
        if (NetworkConnection.isConnectedToNetwork()){
            
            do {
                
                let phone = try getEnteredPhoneNumber()
                let pin = try getEnteredPin()
                let confirmPin = try getEnteredConfirmPin()
                
                if pin != confirmPin{
                    servicemodel.showAlert(title: "Error", message: "Pin does not match", parentView: self)
                    return
                }
                
                HUDIndicatorView.shared.showHUD(view: self.view,
                                                title: "Please wait ..")
                
                DispatchQueue.global(qos: .userInitiated).async {
                    
                    servicemodel.changePassword(newPassword: pin, phoneNumber: phone) { responseStr in
                        print(responseStr)
                        
                        DispatchQueue.main.async {
                            
                            let status = responseStr.value(forKey: "result") as? String
                            
                            if(status == "SUCCESS"){
                                
                                HUDIndicatorView.shared.dismissHUD()
                                
                                Toast.show(message: "Pin reset successfully.", controller: self)
                                
                                DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
                                    
                                    self.navigationController?.popViewController(animated: true)
                                }
                                
                            }
                            else{
                                
                                let message = responseStr.value(forKey: "error_reason") as? String
                                servicemodel.showAlert(title: "Failed to reset", message: message!, parentView: self)
                                
                                HUDIndicatorView.shared.dismissHUD()
                            }
                            
                        }
                    }
                }
                
            } catch {
                
                servicemodel.handleError(error: error, parentView: self)
            }
        }else{
            servicemodel.showAlert(title: "Error", message:"Check Internet Connection", parentView: self)
        }
    }
    
    
    @IBAction func resetPinAction(_ sender: UIButton) {
        
        checkForUsernameExists()
    }
    
    func checkForUsernameExists(){
        
        let service = ServiceModel()
        
        self.phoneTextField.resignFirstResponder()
        
        do {
            
            let phone = try getEnteredPhoneNumber()
            let pin = try getEnteredPin()
            let confirmPin = try getEnteredConfirmPin()
            
            if pin != confirmPin{
                service.showAlert(title: "Error", message: "Pin does not match", parentView: self)
                return
            }
            
            HUDIndicatorView.shared.showHUD(view: self.view,
                                            title: "Please wait ..")
            
            DispatchQueue.global(qos: .userInitiated).async {
                
                service.checkUsernameExists(phoneNumber: phone, isOnlyForPatient: false){
                    responseStr in
                    print(responseStr)
                    
                    DispatchQueue.main.async {
                        
                        let status = responseStr.value(forKey: "result") as? String
                        
                        if(status == "TRUE"){
                            
                            self.sendOTPAPIInvoke(phone: phone)
                            
                        }
                        else{
                            
                            service.showAlert(title: "Phone number does not exist.", message: "", parentView: self)
                            
                            HUDIndicatorView.shared.dismissHUD()
                        }
                    }
                    
                }
                
            }
        }
            
        catch {
            service.handleError(error: error, parentView: self)
        }
    }
    
    func sendOTPAPIInvoke(phone: String){
        
        let service = ServiceModel()
        
        do {
            
            DispatchQueue.global(qos: .userInitiated).async {
                
                service.sendOTP(phone: phone){
                    responseStr in
                    print(responseStr)
                    
                    DispatchQueue.main.async {
                        
                        let message = responseStr.value(forKey: "result") as? String
                        
                        Toast.show(message: message!, controller: self)
                        
                        self.receivedOtp = responseStr.value(forKey: "OTP") as? String
                        
                        // Display OTP input popup
                        self.otpPopupView.isHidden = false
                        self.otpView.initializeOTPFields()
                        
                        // Disbale confirm button
                        self.disbleConfirmButton()
                        
                        HUDIndicatorView.shared.dismissHUD()
                        
                    }
                    
                }
                
            }
        }
            
        catch {
            service.handleError(error: error, parentView: self)
            
            HUDIndicatorView.shared.dismissHUD()
        }
    }
    
    func enableConfirmButton(){
        
        btnConfirmOTP.isEnabled = true
        btnConfirmOTP.backgroundColor = #colorLiteral(red: 0.1490196078, green: 0.4588235294, blue: 0.8196078431, alpha: 1)
        btnConfirmOTP.setTitleColor(#colorLiteral(red: 1, green: 1, blue: 1, alpha: 1), for: .normal)
        
    }
    
    func disbleConfirmButton(){
        
        btnConfirmOTP.isEnabled = false
        btnConfirmOTP.backgroundColor = #colorLiteral(red: 0.8352941176, green: 0.8352941176, blue: 0.8352941176, alpha: 1)
        btnConfirmOTP.setTitleColor(#colorLiteral(red: 1, green: 1, blue: 1, alpha: 1), for: .normal)
    }
    
    @IBAction func backAction(_ sender: UIButton) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func viewTapAction(_ sender: UITapGestureRecognizer) {
        
        sender.cancelsTouchesInView = false
        self.view.endEditing(true)
        
    }
   

}

extension ForgotPasswordVC: UITextFieldDelegate {
    
    
    public func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let allowedCharacters = CharacterSet.decimalDigits
        let characterSet = CharacterSet(charactersIn: string)
        
        if textField == phoneTextField{
        return range.location < 10 && allowedCharacters.isSuperset(of: characterSet)
        }
        else {
           return range.location < 6 && allowedCharacters.isSuperset(of: characterSet)
        }
    }
    
}

extension ForgotPasswordVC: PHOTPViewDelegate
{
    func hasEnteredAllOTP(hasEntered: Bool) -> Bool
    {
        print("Has entered all OTP? \(hasEntered)")
        
        if hasEntered && (enteredOtp == receivedOtp){
            self.enableConfirmButton()
        }
        else{
            self.disbleConfirmButton()
        }
        
        return enteredOtp == receivedOtp
    }
    
    func shouldBecomeFirstResponderForOTP(otpFieldIndex index: Int) -> Bool
    {
        return true
    }
    
    func getenteredOTP(otpString: String) {
        enteredOtp = otpString
        print("OTPString: \(otpString)")
    }
}

extension ForgotPasswordVC
{
    
    func setUpOTPUI()
    {
        setupVarificationPINView()
    }
    
    private func setupVarificationPINView()
    {
        config.otpFieldDisplayType = .square
        config.otpFieldSeparatorSpace = 10
        config.otpFieldSize = 35
        config.otpFieldsCount = 4
        config.otpFieldDefaultBorderColor = #colorLiteral(red: 0.1490196078, green: 0.4588235294, blue: 0.8196078431, alpha: 1)
        config.otpFieldEnteredBorderColor = #colorLiteral(red: 0.9568627451, green: 0.9568627451, blue: 0.9568627451, alpha: 1)
        config.otpFieldErrorBorderColor = #colorLiteral(red: 1, green: 0.06274509804, blue: 0.07450980392, alpha: 1)
        config.otpFieldBorderWidth = 2
        config.shouldAllowIntermediateEditing = false
        
        otpView = PHOTPView(config: config)
        otpView.delegate = self
        
        otpPopupView.addSubview(otpView)
        setConstraintForPinView()
        
        // Create the UI
        otpView.initializeUI()
    }
    
    private func setConstraintForPinView()
    {
        otpView.translatesAutoresizingMaskIntoConstraints = false
        otpView.centerYAnchor.constraint(equalTo: self.view.centerYAnchor,constant: -50).isActive = true
        otpView.heightAnchor.constraint(equalToConstant: 60).isActive = true
        otpView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 30).isActive = true
        otpView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: -30).isActive = true
    }
    
}
