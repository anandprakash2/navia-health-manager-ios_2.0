//
//  InitialVC.swift
//  COPD
//
//  Created by Anand Prakash on 22/10/19.
//  Copyright © 2019 Anand Prakash. All rights reserved.
//

import Foundation
import UIKit
import CoreLocation
import MapKit

class InitialVC: UIViewController {
    
    // MARK: - Declarations
    @IBOutlet weak var btnExistingUser: UIButton!
    @IBOutlet weak var btnNewUser: UIButton!
    
    var locManager = CLLocationManager()
    var currentLocation: CLLocation!
    
    // MARK: - Lifecycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        locManager.requestWhenInUseAuthorization()
        
        if (CLLocationManager.authorizationStatus() == CLAuthorizationStatus.authorizedWhenInUse ||
            CLLocationManager.authorizationStatus() == CLAuthorizationStatus.authorizedAlways){
            guard let currentLocation = locManager.location else {
                return
            }
            print(currentLocation.coordinate.latitude)
            print(currentLocation.coordinate.longitude)
        }
        

    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
    }
   
    
    // MARK: - IBActions
    @IBAction func existingUserAction(_ sender: UIButton) {
        
        // Navigate to login screen
        let loginVC = AppManager.UserStoryBoard.instantiateViewController(withIdentifier: "loginvc") as! LoginVC
        self.navigationController?.pushViewController(loginVC, animated: true)
        
    }
    
    @IBAction func newUserAction(_ sender: UIButton) {
        
        // Navigate to register screen
        let registerVC = AppManager.UserStoryBoard.instantiateViewController(withIdentifier: "registervc") as! RegisterVC
        self.navigationController?.pushViewController(registerVC, animated: true)
    }

}
