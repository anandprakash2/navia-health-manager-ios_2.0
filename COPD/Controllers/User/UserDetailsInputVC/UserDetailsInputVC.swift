//
//  UserDetailsInputVC.swift
//  COPD
//
//  Created by Anand Prakash on 23/10/19.
//  Copyright © 2019 Anand Prakash. All rights reserved.
//

import Foundation
import UIKit
import CoreLocation
import DropDown

class UserDetailsInputVC: UIViewController {
    
    // MARK: - Declarations
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var pinTextField: UITextField!
    @IBOutlet weak var confirmPinTextField: UITextField!
    @IBOutlet weak var genderTextField: UITextField!
    @IBOutlet weak var dobTextField: UITextField!
    
    var dropDown = DropDown()
    var datePicker = UIDatePicker()
    var passingRegisteredMobileNumber: String?
    
    // MARK: - Lifecycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        displayDOBSelectionDatePicker()
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        
    }
    
    func getEnteredName() throws -> String {
        
        guard let name = nameTextField.text,
            name != "" else {
                throw AppError.LoginScreenError.usernameFieldEmpty
        }
        
        if name.count <= 3{
            throw AppError.LoginScreenError.invalidName
        }
        
        return name
    }
    
    
    func getSelectedGender() throws -> String {
        
        guard let selectedGender = genderTextField.text,
            selectedGender != "" else {
                throw AppError.LoginScreenError.genderFieldEmpty
        }
        
        return selectedGender
    }
    
    func getSelectedDOB() throws -> String {
        
        guard let selectedDOB = dobTextField.text,
            selectedDOB != "" else {
                throw AppError.LoginScreenError.dobFieldEmpty
        }
        
        let dateOfBirth = datePicker.date
        let today = NSDate()
        let gregorian = NSCalendar(calendarIdentifier: NSCalendar.Identifier.gregorian)!
        let age = gregorian.components([.year], from: dateOfBirth, to: today as Date, options: [])
        
        if (age.year! < 18) {
            // User's age is under 18
            throw AppError.LoginScreenError.invalidDOB
        }
        
        return selectedDOB
    }
    
    func getEnteredPin() throws -> String {
        
        guard let pin = pinTextField.text,
            pin != "" else {
                throw AppError.LoginScreenError.newPinFieldEmpty
        }
        
        if pin.count < 6{
            throw AppError.LoginScreenError.invalidPin
        }
        
        return pin
    }
    
    func getEnteredConfirmPin() throws -> String {
        
        guard let confirmPin = confirmPinTextField.text,
            confirmPin != "" else {
                throw AppError.LoginScreenError.confirmPinFieldEmpty
        }
        
        return confirmPin
    }
    
    // Change dob format from "dd/MM/yyyy" to "yyyy-MM-dd"

    func convertDateFormater(_ date: String) -> String
    {
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "dd/MM/yyyy"
        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.dateFormat = "yyyy-MM-dd"
        let formatedDate = dateFormatterGet.date(from: date)
        
        return dateFormatterPrint.string(from: formatedDate ?? Date())
    }
    
    // MARK: - IBActions
    
    @IBAction func resetPinAction(_ sender: UIButton) {
        
        let servicemodel = ServiceModel()
        
        if (NetworkConnection.isConnectedToNetwork()){
            
            do {
                let name = try getEnteredName()
                
                let dob = try getSelectedDOB()
                let gender = try getSelectedGender()
                let pin = try getEnteredPin()
                let confirmPin = try getEnteredConfirmPin()
                
                let correctDOBFormat = convertDateFormater(dob)
                print(dob, correctDOBFormat)
                
                if pin != confirmPin{
                    servicemodel.showAlert(title: "Error", message: "Pin does not match", parentView: self)
                    return
                }
                
                // Navigate to Privacy policy screen
                let privacyPolicyVC = AppManager.UserStoryBoard.instantiateViewController(withIdentifier: "privacypolicyvc") as! PrivacyPolicyVC
                privacyPolicyVC.setupVC(name: name, gender: gender, dob: correctDOBFormat, mobileNumber: self.passingRegisteredMobileNumber!, pin: pin)
                self.navigationController?.pushViewController(privacyPolicyVC, animated: true)
                
            } catch {
                
                servicemodel.handleError(error: error, parentView: self)
            }
        }
        else{
            servicemodel.showAlert(title: "Error", message:"Check Internet Connection", parentView: self)
        }
    }
    
    @IBAction func backAction(_ sender: UIButton) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func viewTapAction(_ sender: UITapGestureRecognizer) {
        
        sender.cancelsTouchesInView = false
        self.view.endEditing(true)
        
    }
    
    func displayGenderSelectionPopup(){
        DispatchQueue.main.async {
            self.dropDown.direction = .bottom
            self.dropDown.dataSource = ["Male", "Female", "Others"]
            self.dropDown.anchorView = self.genderTextField
            self.dropDown.textFont = UIFont.init(name: AppFonts.PoppinsRegular, size: 16.0)!
            self.dropDown.bottomOffset =  CGPoint(x: 0, y: (self.dropDown.anchorView?.plainView.bounds.height)!)
            self.dropDown.selectionAction = { (index: Int, item: String) in
                
                if  self.genderTextField.text == item
                {
                    print("Selected same User")
                    
                    
                }else{
                    self.genderTextField.text = item
                }
                
            }
            self.dropDown.show()
            // self.dropDown.hide()
        }
    }
    
    func displayDOBSelectionDatePicker(){
        
        // Formate Date
        datePicker = UIDatePicker(frame: .zero)
        datePicker.datePickerMode = .date
        datePicker.date = Date()
        datePicker.maximumDate = Date()
        
        // Add ToolBar
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        
        // Add done button & cancel button
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(donedatePicker));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelDatePicker));
        
        toolbar.setItems([cancelButton,spaceButton,doneButton], animated: false)
        
        // Add toolbar to dob textField
        dobTextField.inputAccessoryView = toolbar
        
        // Add datepicker to dob textField
        dobTextField.inputView = datePicker
    }
    
    @objc func donedatePicker(){
        
        let formatter = DateFormatter()
        formatter.dateFormat = "dd/MM/yyyy"
        dobTextField.text = formatter.string(from: datePicker.date)
        
        self.view.endEditing(true)
    }
    
    @objc func cancelDatePicker(){
        self.view.endEditing(true)
    }
    
    
}

extension UserDetailsInputVC: UITextFieldDelegate {
    
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == genderTextField {
            self.dismissAllTextFields()
            displayGenderSelectionPopup()
            return false; //do not show keyboard nor cursor
        }
        
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField!) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    public func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let allowedCharacters = CharacterSet.decimalDigits
        let characterSet = CharacterSet(charactersIn: string)
        
        if textField == nameTextField{
            return range.location < 100
        }
        else {
            return range.location < 6 && allowedCharacters.isSuperset(of: characterSet)
        }
    }
    
    func dismissAllTextFields(){
        
        self.view.endEditing(true)
    }
    
}
