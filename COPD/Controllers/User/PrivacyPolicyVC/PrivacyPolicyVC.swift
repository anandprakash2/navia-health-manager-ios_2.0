//
//  PrivacyPolicyVC.swift
//  COPD
//
//  Created by Anand Prakash on 31/10/19.
//  Copyright © 2019 Anand Prakash. All rights reserved.
//

import Foundation
import UIKit

class PrivacyPolicyVC: UIViewController {
    
    // MARK: - IBOutlets
    @IBOutlet weak var readAndAgreeBtn: UIButton!
    @IBOutlet weak var continueBtn: UIButton!
    
    @IBOutlet weak var readAndAgreeBtnImage: UIImageView!
        
    @IBOutlet weak var textView: UITextView!
    
    var name = ""
    var gender = ""
    var phoneNumber = ""
    var dob = ""
    var pin = ""
    
    
    // MARK: - Lifecycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        readAndAgreeBtnImage.image = #imageLiteral(resourceName: "unselect")
        textView.from(html: getHTML())
        checkforContinueButtonEnable()
        
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        UIApplication.shared.sendAction(#selector(UIApplication.resignFirstResponder), to: nil, from: nil, for: nil);
        
    }
    
    func setupVC(name: String, gender: String, dob: String, mobileNumber: String, pin: String) {
        
        self.name = name
        self.gender = gender
        self.dob = dob
        self.phoneNumber = mobileNumber
        self.pin = pin
    }
    
    private func getHTML() -> String {
        var html = ""
        if let htmlPathURL = Bundle.main.url(forResource: "TermConditions", withExtension: "html"){
            do {
                html = try String(contentsOf: htmlPathURL, encoding: .utf8)
            } catch  {
                print("Unable to get the file.")
            }
        }
        
        return html
    }
    
    func checkforContinueButtonEnable(){
        
        if readAndAgreeBtnImage.image == #imageLiteral(resourceName: "select"){
            continueBtn.isEnabled = true
            continueBtn.backgroundColor = #colorLiteral(red: 0.1490196078, green: 0.4588235294, blue: 0.8196078431, alpha: 1)
        }
            
        else{
            continueBtn.isEnabled = false
            continueBtn.backgroundColor = #colorLiteral(red: 0.8352941176, green: 0.8352941176, blue: 0.8352941176, alpha: 1)
        }
    }
    
    
    // MARK: - IBAction
    
    @IBAction func readAndAgreeBtnAction(_ sender: UIButton) {
        
        readAndAgreeBtnImage.image = #imageLiteral(resourceName: "select")
        checkforContinueButtonEnable()
    }
    
    @IBAction func cancelBtnAction(_ sender: UIButton) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func continueBtnAction(_ sender: UIButton) {
        
        createUserAPIFor(userType: "patient")
    }
    
    func createUserAPIFor(userType: String){
        
        let servicemodel = ServiceModel()
        
        if (NetworkConnection.isConnectedToNetwork()){
            
            do {
                
                HUDIndicatorView.shared.showHUD(view: self.view,
                                                title: "Creating account ..")
                
                DispatchQueue.global(qos: .userInitiated).async {
                    
                    servicemodel.createUser(phone: self.phoneNumber, name: self.name, gender: self.gender, dob: self.dob, pin: self.pin) { responseStr in
                        print(responseStr)
                        
                        DispatchQueue.main.async {
                            
                            let status = responseStr.value(forKey: "result") as? String
                            
                            if(status == "SUCCESS"){
                                
                                // Save mobile number as phone number
                                self.loginAction(phone: self.phoneNumber, pin: self.pin)
                                
                            }
                            else{
                                
                                let message = responseStr.value(forKey: "error_reason") as? String
                                servicemodel.showAlert(title: "Failed to register", message: message!, parentView: self)
                                
                                HUDIndicatorView.shared.dismissHUD()
                            }
                        }
                    }
                }
                
            } catch {
                
                servicemodel.handleError(error: error, parentView: self)
            }
        }else{
            servicemodel.showAlert(title: "Error", message:"Check Internet Connection", parentView: self)
        }
    }
    
    
    func loginAction(phone: String, pin: String) {
        
        let servicemodel = ServiceModel()
        
        if (NetworkConnection.isConnectedToNetwork()){
            
            do {
                
                DispatchQueue.global(qos: .userInitiated).async {
                    
                    servicemodel.login(email: phone, password: pin) { responseStr in
                        print(responseStr)
                        
                        DispatchQueue.main.async {
                            
                            if (responseStr.value(forKey: "data") != nil){
                                
                                if let successData = responseStr.value(forKey: "data") as? NSDictionary{
                                    let authToken = successData.value(forKey: "auth_token") as? String
                                    let userName = successData.value(forKey: "for_user") as? String
                                    let linkedPatient = successData.value(forKey: "linked_patient") as? String
                                    
                                    print(authToken!, userName!, linkedPatient)
                                    
                                    // Save to Userdefaults
                                    UserDefaults.standard.set(authToken, forKey: "Authentcation_Token")
                                    
                                    UserDefaults.standard.set(userName, forKey: "Username")
                                    
                                    UserDefaults.standard.synchronize()
                                    
                                    // Navigate to home screen
                                    let homeViewController = AppManager.DashboardStoryBoard.instantiateViewController(withIdentifier: "DashboardViewController") as? DashboardViewController
                                    self.navigationController?.pushViewController(homeViewController!, animated: true)

                                }
                            }
                                
                            else{
                                let message = responseStr.value(forKey: "error_reason") as? String
                                servicemodel.showAlert(title: "Failed to register", message: message!, parentView: self)
                                
                            }
                            
                            HUDIndicatorView.shared.dismissHUD()
                            
                        }
                    }
                }
                
            } catch {
                
                servicemodel.handleError(error: error, parentView: self)
            }
        }else{
            servicemodel.showAlert(title: "Error", message:"Check Internet Connection", parentView: self)
        }
    }
    
}


extension UITextView {
    func from(html: String) {
        if let htmlData = html.data(using: String.Encoding.unicode) {
            do {
                self.attributedText = try NSAttributedString(data: htmlData, options: [
                    NSAttributedString.DocumentReadingOptionKey.documentType: NSAttributedString.DocumentType.html,
                    NSAttributedString.DocumentReadingOptionKey.characterEncoding: String.Encoding.utf8.rawValue
                    ], documentAttributes: nil)
                
            } catch let e as NSError {
                print("Couldn't parse \(html): \(e.localizedDescription)")
            }
        }
    }   
}

