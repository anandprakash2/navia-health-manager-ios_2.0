//
//  LoginVC.swift
//  COPD
//
//  Created by Anand Prakash on 22/10/19.
//  Copyright © 2019 Anand Prakash. All rights reserved.
//

import Foundation
import UIKit


class LoginVC: UIViewController {
    
    // MARK: - Declarations
    @IBOutlet weak var phoneTextField: UITextField!
    @IBOutlet weak var pinTextField: UITextField!
        
    @IBOutlet weak var btnForgotPassword: UIButton!
    
    // MARK: - Lifecycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Adding underline to Forgot password button
        let attributedString1 = NSMutableAttributedString(string: "Forgot Password? Reset Here", attributes: [NSAttributedString.Key.foregroundColor : UIColor.darkGray, NSAttributedString.Key.font:UIFont.init(name: AppFonts.PoppinsRegular, size: 14.0)!])
        
        attributedString1.addAttributes([NSAttributedString.Key.foregroundColor : #colorLiteral(red: 1, green: 0.06407015136, blue: 0.07397313087, alpha: 1),
                                         NSAttributedString.Key.font : UIFont.init(name: AppFonts.PoppinsMedium, size: 14.0)!,
                                         NSAttributedString.Key.underlineStyle: NSUnderlineStyle.single.rawValue as Any],range: NSRange(location:17,length:10))
        
        btnForgotPassword.setAttributedTitle(attributedString1, for: .normal)
        
        // Save loginType to Userdefaults
        UserDefaults.standard.set("patient", forKey: "Logintype")
        UserDefaults.standard.synchronize()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        

    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        
        
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
    }
    
    
    func getEnteredPhoneNumber() throws -> String {
        
        guard let phoneNumber = phoneTextField.text,
            phoneNumber != "" else {
                throw AppError.LoginScreenError.phoneFieldEmpty
        }
        
        if !Utility.isValidPhoneNumber(phoneNumber: phoneNumber) {
            throw AppError.LoginScreenError.invalidPhoneNumber
        }
        
        return phoneNumber
    }
    
    
    func getEnteredPin() throws -> String {
        
        guard let pin = pinTextField.text,
            pin != "" else {
                throw AppError.LoginScreenError.pinFieldEmpty
        }
        
        if  pin.count < 4 {
            throw AppError.LoginScreenError.invalidLoginPin
        }
        
        return pin
    }
    
    
    // MARK: - IBActions
    
    @IBAction func loginAction(_ sender: UIButton) {
        
        let servicemodel = ServiceModel()
        
        if (NetworkConnection.isConnectedToNetwork()){
            
            do {
                let phone = try getEnteredPhoneNumber()
                let pin = try getEnteredPin()
                
                self.phoneTextField.resignFirstResponder()
                self.pinTextField.resignFirstResponder()
                
                HUDIndicatorView.shared.showHUD(view: self.view,
                                                title: "Signing in ..")
                
                DispatchQueue.global(qos: .userInitiated).async {
                    
                    servicemodel.login(email: phone, password: pin) { responseStr in
                        print(responseStr)
                        
                        DispatchQueue.main.async {
                            
                            if (responseStr.value(forKey: "data") != nil){
                                
                                if let successData = responseStr.value(forKey: "data") as? NSDictionary{
                                    let authToken = successData.value(forKey: "auth_token") as? String
                                    let userName = successData.value(forKey: "for_user") as? String
                                    let linkedPatient = successData.value(forKey: "linked_patient") as? String
                                    
                                    print(authToken!, userName!, linkedPatient)
                                    
                                    // Save to Userdefaults
                                    UserDefaults.standard.set(authToken, forKey: "Authentcation_Token")
                                    
                                    UserDefaults.standard.set(userName, forKey: "Username")
                                    
                                    UserDefaults.standard.synchronize()
                                    
                                    // Navigate to home screen
                                    let homeViewController = AppManager.DashboardStoryBoard.instantiateViewController(withIdentifier: "DashboardViewController") as? DashboardViewController
                                self.navigationController?.pushViewController(homeViewController!, animated: true)
                                    
                                }
                            }
                                
                            else{
                                let message = responseStr.value(forKey: "error_reason") as? String
                                servicemodel.showAlert(title: "Login Failed", message: message!, parentView: self)
                                
                            }
                            
                            HUDIndicatorView.shared.dismissHUD()
                            
                        }
                    }
                }
                
            } catch {
                
                servicemodel.handleError(error: error, parentView: self)
            }
        }else{
            servicemodel.showAlert(title: "Error", message:"Check Internet Connection", parentView: self)
        }
    }
    
    
    
    @IBAction func registerAction(_ sender: UIButton) {
        
        // Navigate to register screen
        let registerVC = AppManager.UserStoryBoard.instantiateViewController(withIdentifier: "registervc") as! RegisterVC
        self.navigationController?.pushViewController(registerVC, animated: true)
    }
    
    @IBAction func forgotPasswordAction(_ sender: UIButton) {
        
        // Navigate to forgot password screen
        let forgotpasswordVC = AppManager.UserStoryBoard.instantiateViewController(withIdentifier: "forgotpasswordvc") as! ForgotPasswordVC
        self.navigationController?.pushViewController(forgotpasswordVC, animated: true)
        
    }
    
    @IBAction func viewTapAction(_ sender: UITapGestureRecognizer) {
        
        sender.cancelsTouchesInView = false
        self.view.endEditing(true)
        
    }
    
    
}

extension LoginVC: UITextFieldDelegate {
    
    public func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let allowedCharacters = CharacterSet.decimalDigits
        let characterSet = CharacterSet(charactersIn: string)
        
        if textField == phoneTextField{
            return range.location < 10 && allowedCharacters.isSuperset(of: characterSet)
        }
        else {
            return range.location < 6 && allowedCharacters.isSuperset(of: characterSet)
        }
    }
    
}
